!(function (t, e) {
  'object' == typeof exports && 'object' == typeof module
    ? (module.exports = e())
    : 'function' == typeof define && define.amd
    ? define([], e)
    : 'object' == typeof exports
    ? (exports['vue-js-modal'] = e())
    : (t['vue-js-modal'] = e())
})(global, function () {
  return (
    (i = {}),
    (o.m = n = [
      function (t, e) {
        t.exports = function (n) {
          var a = []
          return (
            (a.toString = function () {
              return this.map(function (t) {
                var e = (function (t, e) {
                  var n = t[1] || '',
                    i = t[3]
                  if (!i) return n
                  if (e && 'function' == typeof btoa) {
                    ;(t = (function (t) {
                      return (
                        '/*# sourceMappingURL=data:application/json;charset=utf-8;base64,' +
                        btoa(unescape(encodeURIComponent(JSON.stringify(t)))) +
                        ' */'
                      )
                    })(i)),
                      (e = i.sources.map(function (t) {
                        return '/*# sourceURL=' + i.sourceRoot + t + ' */'
                      }))
                    return [n].concat(e).concat([t]).join('\n')
                  }
                  return [n].join('\n')
                })(t, n)
                return t[2] ? '@media ' + t[2] + '{' + e + '}' : e
              }).join('')
            }),
            (a.i = function (t, e) {
              'string' == typeof t && (t = [[null, t, '']])
              for (var n = {}, i = 0; i < this.length; i++) {
                var o = this[i][0]
                'number' == typeof o && (n[o] = !0)
              }
              for (i = 0; i < t.length; i++) {
                var r = t[i]
                ;('number' == typeof r[0] && n[r[0]]) ||
                  (e && !r[2] ? (r[2] = e) : e && (r[2] = '(' + r[2] + ') and (' + e + ')'), a.push(r))
              }
            }),
            a
          )
        }
      },
      function (t, e, n) {
        'use strict'
        function i(t, e, n, i) {
          ;(i = !i && 'undefined' != typeof __VUE_SSR_CONTEXT__ ? __VUE_SSR_CONTEXT__ : i) &&
            (i.hasOwnProperty('styles') ||
              (Object.defineProperty(i, 'styles', {
                enumerable: !0,
                get: function () {
                  return o(i._styles)
                }
              }),
              (i._renderStyles = o)),
            (n
              ? function (t, e) {
                  for (var n = 0; n < e.length; n++)
                    for (var i = e[n].parts, o = 0; o < i.length; o++) {
                      var r = i[o],
                        a = r.media || 'default',
                        s = t[a]
                      s
                        ? s.ids.indexOf(r.id) < 0 && (s.ids.push(r.id), (s.css += '\n' + r.css))
                        : (t[a] = { ids: [r.id], css: r.css, media: r.media })
                    }
                }
              : function (t, e) {
                  for (var n = 0; n < e.length; n++)
                    for (var i = e[n].parts, o = 0; o < i.length; o++) {
                      var r = i[o]
                      t[r.id] = { ids: [r.id], css: r.css, media: r.media }
                    }
                })(
              i._styles || (i._styles = {}),
              (e = (function (t, e) {
                for (var n = [], i = {}, o = 0; o < e.length; o++) {
                  var r = e[o],
                    a = r[0],
                    r = { id: t + ':' + o, css: r[1], media: r[2], sourceMap: r[3] }
                  i[a] ? i[a].parts.push(r) : n.push((i[a] = { id: a, parts: [r] }))
                }
                return n
              })(t, e))
            ))
        }
        function o(t) {
          var e,
            n = ''
          for (e in t) {
            var i = t[e]
            n +=
              '<style data-vue-ssr-id="' +
              i.ids.join(' ') +
              '"' +
              (i.media ? ' media="' + i.media + '"' : '') +
              '>' +
              i.css +
              '</style>'
          }
          return n
        }
        n.r(e),
          n.d(e, 'default', function () {
            return i
          })
      },
      function (t, e, n) {
        var i = n(6)
        ;(i = 'string' == typeof i ? [[t.i, i, '']] : i).locals && (t.exports = i.locals)
        var o = n(1).default
        t.exports.__inject__ = function (t) {
          o('27d83796', i, !1, t)
        }
      },
      function (t, e, n) {
        var i = n(8)
        ;(i = 'string' == typeof i ? [[t.i, i, '']] : i).locals && (t.exports = i.locals)
        var o = n(1).default
        t.exports.__inject__ = function (t) {
          o('0e783494', i, !1, t)
        }
      },
      function (t, e, n) {
        var i = n(10)
        ;(i = 'string' == typeof i ? [[t.i, i, '']] : i).locals && (t.exports = i.locals)
        var o = n(1).default
        t.exports.__inject__ = function (t) {
          o('17757f60', i, !1, t)
        }
      },
      function (t, e, n) {
        'use strict'
        n.r(e)
        var i,
          o = n(2)
        for (i in o)
          ['default'].indexOf(i) < 0 &&
            (function (t) {
              n.d(e, t, function () {
                return o[t]
              })
            })(i)
      },
      function (t, e, n) {
        ;(t.exports = n(0)(!1)).push([
          t.i,
          "\n.vue-modal-resizer {\r\n  display: block;\r\n  overflow: hidden;\r\n  position: absolute;\r\n  width: 12px;\r\n  height: 12px;\r\n  right: 0;\r\n  bottom: 0;\r\n  z-index: 9999999;\r\n  background: transparent;\r\n  cursor: se-resize;\n}\n.vue-modal-resizer::after {\r\n  display: block;\r\n  position: absolute;\r\n  content: '';\r\n  background: transparent;\r\n  left: 0;\r\n  top: 0;\r\n  width: 0;\r\n  height: 0;\r\n  border-bottom: 10px solid #ddd;\r\n  border-left: 10px solid transparent;\n}\n.vue-modal-resizer.clicked::after {\r\n  border-bottom: 10px solid #369be9;\n}\r\n",
          ''
        ])
      },
      function (t, e, n) {
        'use strict'
        n.r(e)
        var i,
          o = n(3)
        for (i in o)
          ['default'].indexOf(i) < 0 &&
            (function (t) {
              n.d(e, t, function () {
                return o[t]
              })
            })(i)
      },
      function (t, e, n) {
        ;(t.exports = n(0)(!1)).push([
          t.i,
          '\n.v--modal-block-scroll {\r\n  overflow: hidden;\r\n  width: 100vw;\n}\n.v--modal-overlay {\r\n  position: fixed;\r\n  box-sizing: border-box;\r\n  left: 0;\r\n  top: 0;\r\n  width: 100%;\r\n  height: 100vh;\r\n  background: rgba(0, 0, 0, 0.2);\r\n  z-index: 999;\r\n  opacity: 1;\n}\n.v--modal-overlay.scrollable {\r\n  height: 100%;\r\n  min-height: 100vh;\r\n  overflow-y: auto;\r\n  -webkit-overflow-scrolling: touch;\n}\n.v--modal-overlay .v--modal-background-click {\r\n  width: 100%;\r\n  min-height: 100%;\r\n  height: auto;\n}\n.v--modal-overlay .v--modal-box {\r\n  position: relative;\r\n  overflow: hidden;\r\n  box-sizing: border-box;\n}\n.v--modal-overlay.scrollable .v--modal-box {\r\n  margin-bottom: 2px;\n}\n.v--modal {\r\n  background-color: white;\r\n  text-align: left;\r\n  border-radius: 3px;\r\n  box-shadow: 0 20px 60px -2px rgba(27, 33, 58, 0.4);\r\n  padding: 0;\n}\n.v--modal.v--modal-fullscreen {\r\n  width: 100vw;\r\n  height: 100vh;\r\n  margin: 0;\r\n  left: 0;\r\n  top: 0;\n}\n.v--modal-top-right {\r\n  display: block;\r\n  position: absolute;\r\n  right: 0;\r\n  top: 0;\n}\n.overlay-fade-enter-active,\r\n.overlay-fade-leave-active {\r\n  transition: all 0.2s;\n}\n.overlay-fade-enter,\r\n.overlay-fade-leave-active {\r\n  opacity: 0;\n}\n.nice-modal-fade-enter-active,\r\n.nice-modal-fade-leave-active {\r\n  transition: all 0.4s;\n}\n.nice-modal-fade-enter,\r\n.nice-modal-fade-leave-active {\r\n  opacity: 0;\r\n  transform: translateY(-20px);\n}\r\n',
          ''
        ])
      },
      function (t, e, n) {
        'use strict'
        n.r(e)
        var i,
          o = n(4)
        for (i in o)
          ['default'].indexOf(i) < 0 &&
            (function (t) {
              n.d(e, t, function () {
                return o[t]
              })
            })(i)
      },
      function (t, e, n) {
        ;(t.exports = n(0)(!1)).push([
          t.i,
          '\n.vue-dialog div {\r\n  box-sizing: border-box;\n}\n.vue-dialog .dialog-flex {\r\n  width: 100%;\r\n  height: 100%;\n}\n.vue-dialog .dialog-content {\r\n  flex: 1 0 auto;\r\n  width: 100%;\r\n  padding: 15px;\r\n  font-size: 14px;\n}\n.vue-dialog .dialog-c-title {\r\n  font-weight: 600;\r\n  padding-bottom: 15px;\n}\n.vue-dialog .dialog-c-text {\n}\n.vue-dialog .vue-dialog-buttons {\r\n  display: flex;\r\n  flex: 0 1 auto;\r\n  width: 100%;\r\n  border-top: 1px solid #eee;\n}\n.vue-dialog .vue-dialog-buttons-none {\r\n  width: 100%;\r\n  padding-bottom: 15px;\n}\n.vue-dialog-button {\r\n  font-size: 12px !important;\r\n  background: transparent;\r\n  padding: 0;\r\n  margin: 0;\r\n  border: 0;\r\n  cursor: pointer;\r\n  box-sizing: border-box;\r\n  line-height: 40px;\r\n  height: 40px;\r\n  color: inherit;\r\n  font: inherit;\r\n  outline: none;\n}\n.vue-dialog-button:hover {\r\n  background: rgba(0, 0, 0, 0.01);\n}\n.vue-dialog-button:active {\r\n  background: rgba(0, 0, 0, 0.025);\n}\n.vue-dialog-button:not(:first-of-type) {\r\n  border-left: 1px solid #eee;\n}\r\n',
          ''
        ])
      },
      function (t, e, n) {
        'use strict'
        n.r(e),
          n.d(e, 'getModalsContainer', function () {
            return M
          })
        var i = function () {
            var e = this,
              t = e.$createElement,
              t = e._self._c || t
            return t('transition', { attrs: { name: e.overlayTransition } }, [
              e.visibility.overlay
                ? t(
                    'div',
                    {
                      ref: 'overlay',
                      class: e.overlayClass,
                      attrs: { 'aria-expanded': e.visibility.overlay.toString(), 'data-modal': e.name }
                    },
                    [
                      t(
                        'div',
                        {
                          staticClass: 'v--modal-background-click',
                          on: {
                            mousedown: function (t) {
                              return t.target !== t.currentTarget ? null : e.handleBackgroundClick(t)
                            },
                            touchstart: function (t) {
                              return t.target !== t.currentTarget ? null : e.handleBackgroundClick(t)
                            }
                          }
                        },
                        [
                          t('div', { staticClass: 'v--modal-top-right' }, [e._t('top-right')], 2),
                          e._v(' '),
                          t(
                            'transition',
                            {
                              attrs: { name: e.transition },
                              on: {
                                'before-enter': e.beforeTransitionEnter,
                                'after-enter': e.afterTransitionEnter,
                                'after-leave': e.afterTransitionLeave
                              }
                            },
                            [
                              e.visibility.modal
                                ? t(
                                    'div',
                                    {
                                      ref: 'modal',
                                      class: e.modalClass,
                                      style: e.modalStyle,
                                      attrs: { role: 'dialog', 'aria-modal': 'true' }
                                    },
                                    [
                                      e._t('default'),
                                      e._v(' '),
                                      e.resizable && !e.isAutoHeight
                                        ? t('resizer', {
                                            attrs: {
                                              'min-width': e.minWidth,
                                              'min-height': e.minHeight,
                                              'max-width': e.maxWidth,
                                              'max-height': e.maxHeight
                                            },
                                            on: { resize: e.handleModalResize }
                                          })
                                        : e._e()
                                    ],
                                    2
                                  )
                                : e._e()
                            ]
                          )
                        ],
                        1
                      )
                    ]
                  )
                : e._e()
            ])
          },
          o = function () {
            var t = this.$createElement
            return (this._self._c || t)('div', { class: this.className }, [])
          }
        function r(t, e) {
          return (
            (function (t) {
              if (Array.isArray(t)) return t
            })(t) ||
            (function (t, e) {
              if ('undefined' == typeof Symbol || !(Symbol.iterator in Object(t))) return
              var n = [],
                i = !0,
                o = !1,
                r = void 0
              try {
                for (
                  var a, s = t[Symbol.iterator]();
                  !(i = (a = s.next()).done) && (n.push(a.value), !e || n.length !== e);
                  i = !0
                );
              } catch (t) {
                ;(o = !0), (r = t)
              } finally {
                try {
                  i || null == s.return || s.return()
                } finally {
                  if (o) throw r
                }
              }
              return n
            })(t, e) ||
            (function (t, e) {
              if (!t) return
              if ('string' == typeof t) return a(t, e)
              var n = Object.prototype.toString.call(t).slice(8, -1)
              'Object' === n && t.constructor && (n = t.constructor.name)
              if ('Map' === n || 'Set' === n) return Array.from(t)
              if ('Arguments' === n || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return a(t, e)
            })(t, e) ||
            (function () {
              throw new TypeError(
                'Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.'
              )
            })()
          )
        }
        function a(t, e) {
          ;(null == e || e > t.length) && (e = t.length)
          for (var n = 0, i = new Array(e); n < e; n++) i[n] = t[n]
          return i
        }
        function s(e, t) {
          var n,
            i = Object.keys(e)
          return (
            Object.getOwnPropertySymbols &&
              ((n = Object.getOwnPropertySymbols(e)),
              t &&
                (n = n.filter(function (t) {
                  return Object.getOwnPropertyDescriptor(e, t).enumerable
                })),
              i.push.apply(i, n)),
            i
          )
        }
        function l(e) {
          for (var t = 1; t < arguments.length; t++) {
            var n = null != arguments[t] ? arguments[t] : {}
            t % 2
              ? s(Object(n), !0).forEach(function (t) {
                  u(e, t, n[t])
                })
              : Object.getOwnPropertyDescriptors
              ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(n))
              : s(Object(n)).forEach(function (t) {
                  Object.defineProperty(e, t, Object.getOwnPropertyDescriptor(n, t))
                })
          }
          return e
        }
        function u(t, e, n) {
          return (
            e in t ? Object.defineProperty(t, e, { value: n, enumerable: !0, configurable: !0, writable: !0 }) : (t[e] = n), t
          )
        }
        o._withStripped = i._withStripped = !0
        function c(t, e, n) {
          return n < t ? t : e < n ? e : n
        }
        function d() {
          var t = window.innerWidth,
            e = document.documentElement.clientWidth
          return t && e ? Math.min(t, e) : e || t
        }
        var f = (function (t) {
          var e = 0 < arguments.length && void 0 !== t ? t : 0
          return function () {
            return (e++).toString()
          }
        })()
        function h(t, e, n, i, o, r, a, s) {
          var l,
            u,
            c = 'function' == typeof t ? t.options : t
          return (
            e && ((c.render = e), (c.staticRenderFns = n), (c._compiled = !0)),
            i && (c.functional = !0),
            r && (c._scopeId = 'data-v-' + r),
            a
              ? ((l = function (t) {
                  ;(t =
                    t ||
                    (this.$vnode && this.$vnode.ssrContext) ||
                    (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext)) ||
                    'undefined' == typeof __VUE_SSR_CONTEXT__ ||
                    (t = __VUE_SSR_CONTEXT__),
                    o && o.call(this, t),
                    t && t._registeredComponents && t._registeredComponents.add(a)
                }),
                (c._ssrRegister = l))
              : o &&
                (l = s
                  ? function () {
                      o.call(this, (c.functional ? this.parent : this).$root.$options.shadowRoot)
                    }
                  : o),
            l &&
              (c.functional
                ? ((c._injectStyles = l),
                  (u = c.render),
                  (c.render = function (t, e) {
                    return l.call(e), u(t, e)
                  }))
                : ((s = c.beforeCreate), (c.beforeCreate = s ? [].concat(s, l) : [l]))),
            { exports: t, options: c }
          )
        }
        var p = h(
          {
            name: 'VueJsModalResizer',
            props: {
              minHeight: { type: Number, default: 0 },
              minWidth: { type: Number, default: 0 },
              maxWidth: { type: Number, default: Number.MAX_SAFE_INTEGER },
              maxHeight: { type: Number, default: Number.MAX_SAFE_INTEGER }
            },
            data: function () {
              return { clicked: !1, size: {} }
            },
            mounted: function () {
              this.$el.addEventListener('mousedown', this.start, !1)
            },
            computed: {
              className: function () {
                return { 'vue-modal-resizer': !0, clicked: this.clicked }
              }
            },
            methods: {
              start: function (t) {
                ;(this.clicked = !0),
                  window.addEventListener('mousemove', this.mousemove, !1),
                  window.addEventListener('mouseup', this.stop, !1),
                  t.stopPropagation(),
                  t.preventDefault()
              },
              stop: function () {
                ;(this.clicked = !1),
                  window.removeEventListener('mousemove', this.mousemove, !1),
                  window.removeEventListener('mouseup', this.stop, !1),
                  this.$emit('resize-stop', { element: this.$el.parentElement, size: this.size })
              },
              mousemove: function (t) {
                this.resize(t)
              },
              resize: function (t) {
                var e,
                  n,
                  i,
                  o = this.$el.parentElement
                o &&
                  ((n = t.clientX - o.offsetLeft),
                  (i = t.clientY - o.offsetTop),
                  (e = Math.min(d(), this.maxWidth)),
                  (t = Math.min(window.innerHeight, this.maxHeight)),
                  (n = c(this.minWidth, e, n)),
                  (i = c(this.minHeight, t, i)),
                  (this.size = { width: n, height: i }),
                  (o.style.width = n + 'px'),
                  (o.style.height = i + 'px'),
                  this.$emit('resize', { element: o, size: this.size }))
              }
            }
          },
          o,
          [],
          !1,
          function (t) {
            var e = n(5)
            e.__inject__ && e.__inject__(t)
          },
          null,
          'aca776a4'
        )
        p.options.__file = 'src/Resizer.vue'
        o = p.exports
        function m(t) {
          return (m =
            'function' == typeof Symbol && 'symbol' == typeof Symbol.iterator
              ? function (t) {
                  return typeof t
                }
              : function (t) {
                  return t && 'function' == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype
                    ? 'symbol'
                    : typeof t
                })(t)
        }
        function v(t) {
          switch (m(t)) {
            case 'number':
              return { type: 'px', value: t }
            case 'string':
              return (function (e) {
                if ('auto' === e) return { type: e, value: 0 }
                var t = g.find(function (t) {
                  return t.regexp.test(e)
                })
                return t ? { type: t.name, value: parseFloat(e) } : { type: '', value: e }
              })(t)
            default:
              return { type: '', value: t }
          }
        }
        function b(t) {
          return 'string' != typeof t ? 0 <= t : ('%' === (t = v(t)).type || 'px' === t.type) && 0 < t.value
        }
        var p = '[-+]?[0-9]*.?[0-9]+',
          g = [
            { name: 'px', regexp: new RegExp('^'.concat(p, 'px$')) },
            { name: '%', regexp: new RegExp('^'.concat(p, '%$')) },
            { name: 'px', regexp: new RegExp('^'.concat(p, '$')) }
          ]
        function y(e, t) {
          var n,
            i = Object.keys(e)
          return (
            Object.getOwnPropertySymbols &&
              ((n = Object.getOwnPropertySymbols(e)),
              t &&
                (n = n.filter(function (t) {
                  return Object.getOwnPropertyDescriptor(e, t).enumerable
                })),
              i.push.apply(i, n)),
            i
          )
        }
        function w(i) {
          for (var t = 1; t < arguments.length; t++) {
            var o = null != arguments[t] ? arguments[t] : {}
            t % 2
              ? y(Object(o), !0).forEach(function (t) {
                  var e, n
                  ;(e = i),
                    (t = o[(n = t)]),
                    n in e
                      ? Object.defineProperty(e, n, { value: t, enumerable: !0, configurable: !0, writable: !0 })
                      : (e[n] = t)
                })
              : Object.getOwnPropertyDescriptors
              ? Object.defineProperties(i, Object.getOwnPropertyDescriptors(o))
              : y(Object(o)).forEach(function (t) {
                  Object.defineProperty(i, t, Object.getOwnPropertyDescriptor(o, t))
                })
          }
          return i
        }
        i = h(
          {
            name: 'VueJsModal',
            props: {
              name: { required: !0, type: String },
              delay: { type: Number, default: 0 },
              resizable: { type: Boolean, default: !1 },
              adaptive: { type: Boolean, default: !1 },
              draggable: { type: [Boolean, String], default: !1 },
              scrollable: { type: Boolean, default: !1 },
              reset: { type: Boolean, default: !1 },
              overlayTransition: { type: String, default: 'overlay-fade' },
              transition: { type: String },
              clickToClose: { type: Boolean, default: !0 },
              classes: { type: [String, Array], default: 'v--modal' },
              styles: { type: [String, Array, Object] },
              minWidth: {
                type: Number,
                default: 0,
                validator: function (t) {
                  return 0 <= t
                }
              },
              minHeight: {
                type: Number,
                default: 0,
                validator: function (t) {
                  return 0 <= t
                }
              },
              maxWidth: { type: Number, default: Number.MAX_SAFE_INTEGER },
              maxHeight: { type: Number, default: Number.MAX_SAFE_INTEGER },
              width: { type: [Number, String], default: 600, validator: b },
              height: {
                type: [Number, String],
                default: 300,
                validator: function (t) {
                  return 'auto' === t || b(t)
                }
              },
              pivotX: {
                type: Number,
                default: 0.5,
                validator: function (t) {
                  return 0 <= t && t <= 1
                }
              },
              pivotY: {
                type: Number,
                default: 0.5,
                validator: function (t) {
                  return 0 <= t && t <= 1
                }
              }
            },
            components: { Resizer: o },
            data: function () {
              return {
                visible: !1,
                visibility: { modal: !1, overlay: !1 },
                shift: { left: 0, top: 0 },
                modal: { width: 0, widthType: 'px', height: 0, heightType: 'px', renderedHeight: 0 },
                viewportHeight: 0,
                viewportWidth: 0,
                mutationObserver: null
              }
            },
            created: function () {
              this.setInitialSize()
            },
            beforeMount: function () {
              var t,
                e = this
              $.event.$on('toggle', this.handleToggleEvent),
                window.addEventListener('resize', this.handleWindowResize),
                this.handleWindowResize(),
                this.scrollable &&
                  !this.isAutoHeight &&
                  console.warn(
                    'Modal "'.concat(this.name, '" has scrollable flag set to true ') +
                      'but height is not "auto" ('.concat(this.height, ')')
                  ),
                this.isAutoHeight &&
                  ((t = (function () {
                    if ('undefined' != typeof window)
                      for (var t = ['', 'WebKit', 'Moz', 'O', 'Ms'], e = 0; e < t.length; e++) {
                        var n = t[e] + 'MutationObserver'
                        if (n in window) return window[n]
                      }
                    return !1
                  })())
                    ? (this.mutationObserver = new t(function (t) {
                        e.updateRenderedHeight()
                      }))
                    : console.warn(
                        'MutationObserver was not found. Vue-js-modal automatic resizing relies heavily on MutationObserver. Please make sure to provide shim for it.'
                      )),
                this.clickToClose && window.addEventListener('keyup', this.handleEscapeKeyUp)
            },
            beforeDestroy: function () {
              $.event.$off('toggle', this.handleToggleEvent),
                window.removeEventListener('resize', this.handleWindowResize),
                this.clickToClose && window.removeEventListener('keyup', this.handleEscapeKeyUp),
                this.scrollable && document.body.classList.remove('v--modal-block-scroll')
            },
            computed: {
              isAutoHeight: function () {
                return 'auto' === this.modal.heightType
              },
              position: function () {
                var t = this.viewportHeight,
                  e = this.viewportWidth,
                  n = this.shift,
                  i = this.pivotX,
                  o = this.pivotY,
                  r = this.trueModalWidth,
                  a = this.trueModalHeight,
                  r = e - r,
                  a = Math.max(t - a, 0),
                  i = n.left + i * r,
                  o = n.top + o * a
                return { left: parseInt(c(0, r, i)), top: parseInt(c(0, a, o)) }
              },
              trueModalWidth: function () {
                var t = this.viewportWidth,
                  e = this.modal,
                  n = this.adaptive,
                  i = this.minWidth,
                  o = this.maxWidth,
                  e = '%' === e.widthType ? (t / 100) * e.width : e.width,
                  o = Math.max(i, Math.min(t, o))
                return n ? c(i, o, e) : e
              },
              trueModalHeight: function () {
                var t = this.viewportHeight,
                  e = this.modal,
                  n = this.isAutoHeight,
                  i = this.adaptive,
                  o = this.minHeight,
                  r = this.maxHeight,
                  e = '%' === e.heightType ? (t / 100) * e.height : e.height
                if (n) return this.modal.renderedHeight
                r = Math.max(o, Math.min(t, r))
                return i ? c(o, r, e) : e
              },
              overlayClass: function () {
                return { 'v--modal-overlay': !0, scrollable: this.scrollable && this.isAutoHeight }
              },
              modalClass: function () {
                return ['v--modal-box', this.classes]
              },
              stylesProp: function () {
                return 'string' == typeof this.styles
                  ? this.styles
                      .split(';')
                      .map(function (t) {
                        return t.trim()
                      })
                      .filter(Boolean)
                      .map(function (t) {
                        return t.split(':')
                      })
                      .reduce(function (t, e) {
                        var n = r(e, 2),
                          e = n[0],
                          n = n[1]
                        return l(l({}, t), {}, u({}, e, n))
                      }, {})
                  : this.styles
              },
              modalStyle: function () {
                return [
                  this.stylesProp,
                  {
                    top: this.position.top + 'px',
                    left: this.position.left + 'px',
                    width: this.trueModalWidth + 'px',
                    height: this.isAutoHeight ? 'auto' : this.trueModalHeight + 'px'
                  }
                ]
              }
            },
            watch: {
              visible: function (t) {
                var e = this
                t
                  ? ((this.visibility.overlay = !0),
                    setTimeout(function () {
                      ;(e.visibility.modal = !0),
                        e.$nextTick(function () {
                          e.addDraggableListeners(), e.callAfterEvent(!0)
                        })
                    }, this.delay))
                  : ((this.visibility.modal = !1),
                    setTimeout(function () {
                      ;(e.visibility.overlay = !1),
                        e.$nextTick(function () {
                          e.removeDraggableListeners(), e.callAfterEvent(!1)
                        })
                    }, this.delay))
              }
            },
            methods: {
              handleToggleEvent: function (t, e, n) {
                this.name === t && ((e = void 0 === e ? !this.visible : e), this.toggle(e, n))
              },
              setInitialSize: function () {
                var t = this.modal,
                  e = v(this.width),
                  n = v(this.height)
                ;(t.width = e.value), (t.widthType = e.type), (t.height = n.value), (t.heightType = n.type)
              },
              handleEscapeKeyUp: function (t) {
                27 === t.which && this.visible && this.$modal.hide(this.name)
              },
              handleWindowResize: function () {
                ;(this.viewportWidth = d()), (this.viewportHeight = window.innerHeight), this.ensureShiftInWindowBounds()
              },
              createModalEvent: function () {
                var t = 0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : {}
                return (function () {
                  var t = 0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : {}
                  return l({ id: f(), timestamp: Date.now(), canceled: !1 }, t)
                })(w({ name: this.name, ref: this.$refs.modal }, t))
              },
              handleModalResize: function (t) {
                ;(this.modal.widthType = 'px'),
                  (this.modal.width = t.size.width),
                  (this.modal.heightType = 'px'),
                  (this.modal.height = t.size.height)
                t = this.modal.size
                this.$emit('resize', this.createModalEvent({ size: t }))
              },
              toggle: function (t, e) {
                var n,
                  i = this.reset,
                  o = this.scrollable,
                  r = this.visible
                r !== t &&
                  ('before-open' == (r = r ? 'before-close' : 'before-open')
                    ? ('undefined' != typeof document &&
                        document.activeElement &&
                        'BODY' !== document.activeElement.tagName &&
                        document.activeElement.blur &&
                        document.activeElement.blur(),
                      i && (this.setInitialSize(), (this.shift.left = 0), (this.shift.top = 0)),
                      o && document.body.classList.add('v--modal-block-scroll'))
                    : o && document.body.classList.remove('v--modal-block-scroll'),
                  (n = !1),
                  (e = this.createModalEvent({
                    stop: function () {
                      n = !0
                    },
                    state: t,
                    params: e
                  })),
                  this.$emit(r, e),
                  n || (this.visible = t))
              },
              getDraggableElement: function () {
                var t = 'string' != typeof this.draggable ? '.v--modal-box' : this.draggable
                return t ? this.$refs.overlay.querySelector(t) : null
              },
              handleBackgroundClick: function () {
                this.clickToClose && this.toggle(!1)
              },
              callAfterEvent: function (t) {
                t ? this.connectObserver() : this.disconnectObserver()
                var e = t ? 'opened' : 'closed',
                  t = this.createModalEvent({ state: t })
                this.$emit(e, t)
              },
              addDraggableListeners: function () {
                var t,
                  i,
                  o,
                  r,
                  a,
                  s,
                  e,
                  n,
                  l,
                  u = this
                !this.draggable ||
                  ((t = this.getDraggableElement()) &&
                    ((a = r = o = i = 0),
                    (s = function (t) {
                      return t.touches && 0 < t.touches.length ? t.touches[0] : t
                    }),
                    (e = function (t) {
                      var e = t.target
                      ;(e && ('INPUT' === e.nodeName || 'TEXTAREA' === e.nodeName || 'SELECT' === e.nodeName)) ||
                        ((t = (e = s(t)).clientX),
                        (e = e.clientY),
                        document.addEventListener('mousemove', n),
                        document.addEventListener('touchmove', n),
                        document.addEventListener('mouseup', l),
                        document.addEventListener('touchend', l),
                        (i = t),
                        (o = e),
                        (r = u.shift.left),
                        (a = u.shift.top))
                    }),
                    (n = function (t) {
                      var e = s(t),
                        n = e.clientX,
                        e = e.clientY
                      ;(u.shift.left = r + n - i), (u.shift.top = a + e - o), t.preventDefault()
                    }),
                    (l = function t(e) {
                      u.ensureShiftInWindowBounds(),
                        document.removeEventListener('mousemove', n),
                        document.removeEventListener('touchmove', n),
                        document.removeEventListener('mouseup', t),
                        document.removeEventListener('touchend', t),
                        e.preventDefault()
                    }),
                    t.addEventListener('mousedown', e),
                    t.addEventListener('touchstart', e)))
              },
              removeDraggableListeners: function () {},
              updateRenderedHeight: function () {
                this.$refs.modal && (this.modal.renderedHeight = this.$refs.modal.getBoundingClientRect().height)
              },
              connectObserver: function () {
                this.mutationObserver &&
                  this.mutationObserver.observe(this.$refs.overlay, { childList: !0, attributes: !0, subtree: !0 })
              },
              disconnectObserver: function () {
                this.mutationObserver && this.mutationObserver.disconnect()
              },
              beforeTransitionEnter: function () {
                this.connectObserver()
              },
              afterTransitionEnter: function () {},
              afterTransitionLeave: function () {},
              ensureShiftInWindowBounds: function () {
                var t = this.viewportHeight,
                  e = this.viewportWidth,
                  n = this.shift,
                  i = this.pivotX,
                  o = this.pivotY,
                  r = this.trueModalWidth,
                  a = this.trueModalHeight,
                  r = e - r,
                  a = Math.max(t - a, 0),
                  i = n.left + i * r,
                  o = n.top + o * a
                ;(this.shift.left -= i - c(0, r, i)), (this.shift.top -= o - c(0, a, o))
              }
            }
          },
          i,
          [],
          !1,
          function (t) {
            var e = n(7)
            e.__inject__ && e.__inject__(t)
          },
          null,
          '66524b9d'
        )
        i.options.__file = 'src/Modal.vue'
        var _ = i.exports,
          i = function () {
            var n = this,
              t = n.$createElement,
              i = n._self._c || t
            return i(
              'modal',
              {
                attrs: {
                  name: 'dialog',
                  height: 'auto',
                  classes: ['v--modal', 'vue-dialog', this.params.class],
                  width: n.width,
                  'pivot-y': 0.3,
                  adaptive: !0,
                  clickToClose: n.clickToClose,
                  transition: n.transition
                },
                on: {
                  'before-open': n.beforeOpened,
                  'before-close': n.beforeClosed,
                  opened: function (t) {
                    return n.$emit('opened', t)
                  },
                  closed: function (t) {
                    return n.$emit('closed', t)
                  }
                }
              },
              [
                i(
                  'div',
                  { staticClass: 'dialog-content' },
                  [
                    n.params.title
                      ? i('div', { staticClass: 'dialog-c-title', domProps: { innerHTML: n._s(n.params.title || '') } })
                      : n._e(),
                    n._v(' '),
                    n.params.component
                      ? i(n.params.component, n._b({ tag: 'component' }, 'component', n.params.props, !1))
                      : i('div', { staticClass: 'dialog-c-text', domProps: { innerHTML: n._s(n.params.text || '') } })
                  ],
                  1
                ),
                n._v(' '),
                n.buttons
                  ? i(
                      'div',
                      { staticClass: 'vue-dialog-buttons' },
                      n._l(n.buttons, function (t, e) {
                        return i(
                          'button',
                          {
                            key: e,
                            class: t.class || 'vue-dialog-button',
                            style: n.buttonStyle,
                            attrs: { type: 'button' },
                            domProps: { innerHTML: n._s(t.title) },
                            on: {
                              click: function (t) {
                                return t.stopPropagation(), n.click(e, t)
                              }
                            }
                          },
                          [n._v('\n      ' + n._s(t.title) + '\n    ')]
                        )
                      }),
                      0
                    )
                  : i('div', { staticClass: 'vue-dialog-buttons-none' })
              ]
            )
          }
        i._withStripped = !0
        i = h(
          {
            name: 'VueJsDialog',
            props: {
              width: { type: [Number, String], default: 400 },
              clickToClose: { type: Boolean, default: !0 },
              transition: { type: String, default: 'fade' }
            },
            data: function () {
              return { params: {}, defaultButtons: [{ title: 'CLOSE' }] }
            },
            computed: {
              buttons: function () {
                return this.params.buttons || this.defaultButtons
              },
              buttonStyle: function () {
                return { flex: '1 1 '.concat(100 / this.buttons.length, '%') }
              }
            },
            methods: {
              beforeOpened: function (t) {
                window.addEventListener('keyup', this.onKeyUp), (this.params = t.params || {}), this.$emit('before-opened', t)
              },
              beforeClosed: function (t) {
                window.removeEventListener('keyup', this.onKeyUp), (this.params = {}), this.$emit('before-closed', t)
              },
              click: function (t, e) {
                var n = 2 < arguments.length && void 0 !== arguments[2] ? arguments[2] : 'click',
                  i = this.buttons[t]
                i && 'function' == typeof i.handler ? i.handler(t, e, { source: n }) : this.$modal.hide('dialog')
              },
              onKeyUp: function (t) {
                var e
                13 === t.which &&
                  0 < this.buttons.length &&
                  -1 !==
                    (e =
                      1 === this.buttons.length
                        ? 0
                        : this.buttons.findIndex(function (t) {
                            return t.default
                          })) &&
                  this.click(e, t, 'keypress')
              }
            }
          },
          i,
          [],
          !1,
          function (t) {
            var e = n(9)
            e.__inject__ && e.__inject__(t)
          },
          null,
          '6a29aa08'
        )
        i.options.__file = 'src/Dialog.vue'
        var x = i.exports,
          i = function () {
            var n = this,
              t = n.$createElement,
              i = n._self._c || t
            return i(
              'div',
              { attrs: { id: 'modals-container' } },
              n._l(n.modals, function (e) {
                return i(
                  'modal',
                  n._g(
                    n._b(
                      {
                        key: e.id,
                        on: {
                          closed: function (t) {
                            return n.remove(e.id)
                          }
                        }
                      },
                      'modal',
                      e.modalAttrs,
                      !1
                    ),
                    e.modalListeners
                  ),
                  [
                    i(
                      e.component,
                      n._g(
                        n._b(
                          {
                            tag: 'component',
                            on: {
                              close: function (t) {
                                return n.$modal.hide(e.modalAttrs.name, t)
                              }
                            }
                          },
                          'component',
                          e.componentAttrs,
                          !1
                        ),
                        n.$listeners
                      )
                    )
                  ],
                  1
                )
              }),
              1
            )
          }
        function O(e, t) {
          var n,
            i = Object.keys(e)
          return (
            Object.getOwnPropertySymbols &&
              ((n = Object.getOwnPropertySymbols(e)),
              t &&
                (n = n.filter(function (t) {
                  return Object.getOwnPropertyDescriptor(e, t).enumerable
                })),
              i.push.apply(i, n)),
            i
          )
        }
        function j(i) {
          for (var t = 1; t < arguments.length; t++) {
            var o = null != arguments[t] ? arguments[t] : {}
            t % 2
              ? O(Object(o), !0).forEach(function (t) {
                  var e, n
                  ;(e = i),
                    (t = o[(n = t)]),
                    n in e
                      ? Object.defineProperty(e, n, { value: t, enumerable: !0, configurable: !0, writable: !0 })
                      : (e[n] = t)
                })
              : Object.getOwnPropertyDescriptors
              ? Object.defineProperties(i, Object.getOwnPropertyDescriptors(o))
              : O(Object(o)).forEach(function (t) {
                  Object.defineProperty(i, t, Object.getOwnPropertyDescriptor(o, t))
                })
          }
          return i
        }
        i._withStripped = !0
        i = h(
          {
            data: function () {
              return { modals: [] }
            },
            created: function () {
              this.$root._dynamicContainer = this
            },
            methods: {
              add: function (t) {
                var e = this,
                  n = 1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : {},
                  i = 2 < arguments.length && void 0 !== arguments[2] ? arguments[2] : {},
                  o = 3 < arguments.length && void 0 !== arguments[3] ? arguments[3] : {},
                  r = f(),
                  a = i.name || '_dynamic_modal_' + r
                this.modals.push({
                  id: r,
                  modalAttrs: j(j({}, i), {}, { name: a }),
                  modalListeners: o,
                  component: t,
                  componentAttrs: n
                }),
                  this.$nextTick(function () {
                    e.$modal.show(a)
                  })
              },
              remove: function (e) {
                var t = this.modals.findIndex(function (t) {
                  return t.id === e
                })
                ;-1 !== t && this.modals.splice(t, 1)
              }
            }
          },
          i,
          [],
          !1,
          null,
          null,
          '699720aa'
        )
        i.options.__file = 'src/ModalsContainer.vue'
        var E = i.exports
        function S(t) {
          return (S =
            'function' == typeof Symbol && 'symbol' == typeof Symbol.iterator
              ? function (t) {
                  return typeof t
                }
              : function (t) {
                  return t && 'function' == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype
                    ? 'symbol'
                    : typeof t
                })(t)
        }
        function k(e, t) {
          var n,
            i = Object.keys(e)
          return (
            Object.getOwnPropertySymbols &&
              ((n = Object.getOwnPropertySymbols(e)),
              t &&
                (n = n.filter(function (t) {
                  return Object.getOwnPropertyDescriptor(e, t).enumerable
                })),
              i.push.apply(i, n)),
            i
          )
        }
        function T(i) {
          for (var t = 1; t < arguments.length; t++) {
            var o = null != arguments[t] ? arguments[t] : {}
            t % 2
              ? k(Object(o), !0).forEach(function (t) {
                  var e, n
                  ;(e = i),
                    (t = o[(n = t)]),
                    n in e
                      ? Object.defineProperty(e, n, { value: t, enumerable: !0, configurable: !0, writable: !0 })
                      : (e[n] = t)
                })
              : Object.getOwnPropertyDescriptors
              ? Object.defineProperties(i, Object.getOwnPropertyDescriptors(o))
              : k(Object(o)).forEach(function (t) {
                  Object.defineProperty(i, t, Object.getOwnPropertyDescriptor(o, t))
                })
          }
          return i
        }
        var M = function (t, e, n) {
            return (
              !n._dynamicContainer &&
                e.injectModalsContainer &&
                ((e = document.createElement('div')),
                document.body.appendChild(e),
                (e = e),
                new t({
                  parent: n,
                  render: function (t) {
                    return t(E)
                  }
                }).$mount(e)),
              n._dynamicContainer
            )
          },
          P = {
            install: function (r) {
              var t,
                a,
                s = 1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : {}
              this.installed ||
                ((this.installed = !0),
                (this.event = new r()),
                (this.rootInstance = null),
                (t = s.componentName || 'Modal'),
                (a = s.dynamicDefaults || {}),
                (r.prototype.$modal = {
                  show: function (t) {
                    for (var e = arguments.length, n = new Array(1 < e ? e - 1 : 0), i = 1; i < e; i++) n[i - 1] = arguments[i]
                    switch (S(t)) {
                      case 'string':
                        return function (t, e) {
                          P.event.$emit('toggle', t, !0, e)
                        }.apply(void 0, [t].concat(n))
                      case 'object':
                      case 'function':
                        return s.dynamic
                          ? function (t, e, n, i) {
                              var o = n && n.root ? n.root : P.rootInstance,
                                o = M(r, s, o)
                              o
                                ? o.add(t, e, T(T({}, a), n), i)
                                : console.warn(
                                    '[vue-js-modal] In order to render dynamic modals, a <modals-container> component must be present on the page.'
                                  )
                            }.apply(void 0, [t].concat(n))
                          : console.warn(
                              '[vue-js-modal] $modal() received object as a first argument, but dynamic modals are switched off. https://github.com/euvl/vue-js-modal/#dynamic-modals'
                            )
                      default:
                        console.warn('[vue-js-modal] $modal() received an unsupported argument as a first argument.', t)
                    }
                  },
                  hide: function (t, e) {
                    P.event.$emit('toggle', t, !1, e)
                  },
                  toggle: function (t, e) {
                    P.event.$emit('toggle', t, void 0, e)
                  }
                }),
                r.component(t, _),
                s.dialog && r.component('VDialog', x),
                s.dynamic &&
                  (r.component('ModalsContainer', E),
                  r.mixin({
                    beforeMount: function () {
                      null === P.rootInstance && (P.rootInstance = this.$root)
                    }
                  })))
            }
          },
          $ = (e.default = P)
      }
    ]),
    (o.c = i),
    (o.d = function (t, e, n) {
      o.o(t, e) || Object.defineProperty(t, e, { enumerable: !0, get: n })
    }),
    (o.r = function (t) {
      'undefined' != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, { value: 'Module' }),
        Object.defineProperty(t, '__esModule', { value: !0 })
    }),
    (o.t = function (e, t) {
      if ((1 & t && (e = o(e)), 8 & t)) return e
      if (4 & t && 'object' == typeof e && e && e.__esModule) return e
      var n = Object.create(null)
      if ((o.r(n), Object.defineProperty(n, 'default', { enumerable: !0, value: e }), 2 & t && 'string' != typeof e))
        for (var i in e)
          o.d(
            n,
            i,
            function (t) {
              return e[t]
            }.bind(null, i)
          )
      return n
    }),
    (o.n = function (t) {
      var e =
        t && t.__esModule
          ? function () {
              return t.default
            }
          : function () {
              return t
            }
      return o.d(e, 'a', e), e
    }),
    (o.o = function (t, e) {
      return Object.prototype.hasOwnProperty.call(t, e)
    }),
    (o.p = '/dist/'),
    o((o.s = 11))
  )
  function o(t) {
    if (i[t]) return i[t].exports
    var e = (i[t] = { i: t, l: !1, exports: {} })
    return n[t].call(e.exports, e, e.exports, o), (e.l = !0), e.exports
  }
  var n, i
})
