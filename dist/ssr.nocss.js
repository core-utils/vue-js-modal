!(function (t, e) {
  'object' == typeof exports && 'object' == typeof module
    ? (module.exports = e())
    : 'function' == typeof define && define.amd
    ? define([], e)
    : 'object' == typeof exports
    ? (exports['vue-js-modal'] = e())
    : (t['vue-js-modal'] = e())
})(global, function () {
  return (
    (i = {}),
    (o.m = n = [
      function (t, e, n) {},
      function (t, e, n) {},
      function (t, e, n) {},
      function (t, e, n) {
        'use strict'
        n.r(e)
        var i,
          o = n(0)
        for (i in o)
          ['default'].indexOf(i) < 0 &&
            (function (t) {
              n.d(e, t, function () {
                return o[t]
              })
            })(i)
      },
      function (t, e, n) {
        'use strict'
        n.r(e)
        var i,
          o = n(1)
        for (i in o)
          ['default'].indexOf(i) < 0 &&
            (function (t) {
              n.d(e, t, function () {
                return o[t]
              })
            })(i)
      },
      function (t, e, n) {
        'use strict'
        n.r(e)
        var i,
          o = n(2)
        for (i in o)
          ['default'].indexOf(i) < 0 &&
            (function (t) {
              n.d(e, t, function () {
                return o[t]
              })
            })(i)
      },
      function (t, e, n) {
        'use strict'
        n.r(e),
          n.d(e, 'getModalsContainer', function () {
            return P
          })
        var i = function () {
            var e = this,
              t = e.$createElement,
              t = e._self._c || t
            return t('transition', { attrs: { name: e.overlayTransition } }, [
              e.visibility.overlay
                ? t(
                    'div',
                    {
                      ref: 'overlay',
                      class: e.overlayClass,
                      attrs: { 'aria-expanded': e.visibility.overlay.toString(), 'data-modal': e.name }
                    },
                    [
                      t(
                        'div',
                        {
                          staticClass: 'v--modal-background-click',
                          on: {
                            mousedown: function (t) {
                              return t.target !== t.currentTarget ? null : e.handleBackgroundClick(t)
                            },
                            touchstart: function (t) {
                              return t.target !== t.currentTarget ? null : e.handleBackgroundClick(t)
                            }
                          }
                        },
                        [
                          t('div', { staticClass: 'v--modal-top-right' }, [e._t('top-right')], 2),
                          e._v(' '),
                          t(
                            'transition',
                            {
                              attrs: { name: e.transition },
                              on: {
                                'before-enter': e.beforeTransitionEnter,
                                'after-enter': e.afterTransitionEnter,
                                'after-leave': e.afterTransitionLeave
                              }
                            },
                            [
                              e.visibility.modal
                                ? t(
                                    'div',
                                    {
                                      ref: 'modal',
                                      class: e.modalClass,
                                      style: e.modalStyle,
                                      attrs: { role: 'dialog', 'aria-modal': 'true' }
                                    },
                                    [
                                      e._t('default'),
                                      e._v(' '),
                                      e.resizable && !e.isAutoHeight
                                        ? t('resizer', {
                                            attrs: {
                                              'min-width': e.minWidth,
                                              'min-height': e.minHeight,
                                              'max-width': e.maxWidth,
                                              'max-height': e.maxHeight
                                            },
                                            on: { resize: e.handleModalResize }
                                          })
                                        : e._e()
                                    ],
                                    2
                                  )
                                : e._e()
                            ]
                          )
                        ],
                        1
                      )
                    ]
                  )
                : e._e()
            ])
          },
          o = function () {
            var t = this.$createElement
            return (this._self._c || t)('div', { class: this.className }, [])
          }
        function r(t, e) {
          return (
            (function (t) {
              if (Array.isArray(t)) return t
            })(t) ||
            (function (t, e) {
              if ('undefined' == typeof Symbol || !(Symbol.iterator in Object(t))) return
              var n = [],
                i = !0,
                o = !1,
                r = void 0
              try {
                for (
                  var a, s = t[Symbol.iterator]();
                  !(i = (a = s.next()).done) && (n.push(a.value), !e || n.length !== e);
                  i = !0
                );
              } catch (t) {
                ;(o = !0), (r = t)
              } finally {
                try {
                  i || null == s.return || s.return()
                } finally {
                  if (o) throw r
                }
              }
              return n
            })(t, e) ||
            (function (t, e) {
              if (!t) return
              if ('string' == typeof t) return a(t, e)
              var n = Object.prototype.toString.call(t).slice(8, -1)
              'Object' === n && t.constructor && (n = t.constructor.name)
              if ('Map' === n || 'Set' === n) return Array.from(t)
              if ('Arguments' === n || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return a(t, e)
            })(t, e) ||
            (function () {
              throw new TypeError(
                'Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.'
              )
            })()
          )
        }
        function a(t, e) {
          ;(null == e || e > t.length) && (e = t.length)
          for (var n = 0, i = new Array(e); n < e; n++) i[n] = t[n]
          return i
        }
        function s(e, t) {
          var n,
            i = Object.keys(e)
          return (
            Object.getOwnPropertySymbols &&
              ((n = Object.getOwnPropertySymbols(e)),
              t &&
                (n = n.filter(function (t) {
                  return Object.getOwnPropertyDescriptor(e, t).enumerable
                })),
              i.push.apply(i, n)),
            i
          )
        }
        function l(e) {
          for (var t = 1; t < arguments.length; t++) {
            var n = null != arguments[t] ? arguments[t] : {}
            t % 2
              ? s(Object(n), !0).forEach(function (t) {
                  u(e, t, n[t])
                })
              : Object.getOwnPropertyDescriptors
              ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(n))
              : s(Object(n)).forEach(function (t) {
                  Object.defineProperty(e, t, Object.getOwnPropertyDescriptor(n, t))
                })
          }
          return e
        }
        function u(t, e, n) {
          return (
            e in t ? Object.defineProperty(t, e, { value: n, enumerable: !0, configurable: !0, writable: !0 }) : (t[e] = n), t
          )
        }
        o._withStripped = i._withStripped = !0
        function c(t, e, n) {
          return n < t ? t : e < n ? e : n
        }
        function d() {
          var t = window.innerWidth,
            e = document.documentElement.clientWidth
          return t && e ? Math.min(t, e) : e || t
        }
        var h = (function (t) {
          var e = 0 < arguments.length && void 0 !== t ? t : 0
          return function () {
            return (e++).toString()
          }
        })()
        function f(t, e, n, i, o, r, a, s) {
          var l,
            u,
            c = 'function' == typeof t ? t.options : t
          return (
            e && ((c.render = e), (c.staticRenderFns = n), (c._compiled = !0)),
            i && (c.functional = !0),
            r && (c._scopeId = 'data-v-' + r),
            a
              ? ((l = function (t) {
                  ;(t =
                    t ||
                    (this.$vnode && this.$vnode.ssrContext) ||
                    (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext)) ||
                    'undefined' == typeof __VUE_SSR_CONTEXT__ ||
                    (t = __VUE_SSR_CONTEXT__),
                    o && o.call(this, t),
                    t && t._registeredComponents && t._registeredComponents.add(a)
                }),
                (c._ssrRegister = l))
              : o &&
                (l = s
                  ? function () {
                      o.call(this, (c.functional ? this.parent : this).$root.$options.shadowRoot)
                    }
                  : o),
            l &&
              (c.functional
                ? ((c._injectStyles = l),
                  (u = c.render),
                  (c.render = function (t, e) {
                    return l.call(e), u(t, e)
                  }))
                : ((s = c.beforeCreate), (c.beforeCreate = s ? [].concat(s, l) : [l]))),
            { exports: t, options: c }
          )
        }
        var p = f(
          {
            name: 'VueJsModalResizer',
            props: {
              minHeight: { type: Number, default: 0 },
              minWidth: { type: Number, default: 0 },
              maxWidth: { type: Number, default: Number.MAX_SAFE_INTEGER },
              maxHeight: { type: Number, default: Number.MAX_SAFE_INTEGER }
            },
            data: function () {
              return { clicked: !1, size: {} }
            },
            mounted: function () {
              this.$el.addEventListener('mousedown', this.start, !1)
            },
            computed: {
              className: function () {
                return { 'vue-modal-resizer': !0, clicked: this.clicked }
              }
            },
            methods: {
              start: function (t) {
                ;(this.clicked = !0),
                  window.addEventListener('mousemove', this.mousemove, !1),
                  window.addEventListener('mouseup', this.stop, !1),
                  t.stopPropagation(),
                  t.preventDefault()
              },
              stop: function () {
                ;(this.clicked = !1),
                  window.removeEventListener('mousemove', this.mousemove, !1),
                  window.removeEventListener('mouseup', this.stop, !1),
                  this.$emit('resize-stop', { element: this.$el.parentElement, size: this.size })
              },
              mousemove: function (t) {
                this.resize(t)
              },
              resize: function (t) {
                var e,
                  n,
                  i,
                  o = this.$el.parentElement
                o &&
                  ((n = t.clientX - o.offsetLeft),
                  (i = t.clientY - o.offsetTop),
                  (e = Math.min(d(), this.maxWidth)),
                  (t = Math.min(window.innerHeight, this.maxHeight)),
                  (n = c(this.minWidth, e, n)),
                  (i = c(this.minHeight, t, i)),
                  (this.size = { width: n, height: i }),
                  (o.style.width = n + 'px'),
                  (o.style.height = i + 'px'),
                  this.$emit('resize', { element: o, size: this.size }))
              }
            }
          },
          o,
          [],
          !1,
          function (t) {
            var e = n(3)
            e.__inject__ && e.__inject__(t)
          },
          null,
          'aca776a4'
        )
        p.options.__file = 'src/Resizer.vue'
        o = p.exports
        function m(t) {
          return (m =
            'function' == typeof Symbol && 'symbol' == typeof Symbol.iterator
              ? function (t) {
                  return typeof t
                }
              : function (t) {
                  return t && 'function' == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype
                    ? 'symbol'
                    : typeof t
                })(t)
        }
        function v(t) {
          switch (m(t)) {
            case 'number':
              return { type: 'px', value: t }
            case 'string':
              return (function (e) {
                if ('auto' === e) return { type: e, value: 0 }
                var t = y.find(function (t) {
                  return t.regexp.test(e)
                })
                return t ? { type: t.name, value: parseFloat(e) } : { type: '', value: e }
              })(t)
            default:
              return { type: '', value: t }
          }
        }
        function b(t) {
          return 'string' != typeof t ? 0 <= t : ('%' === (t = v(t)).type || 'px' === t.type) && 0 < t.value
        }
        var p = '[-+]?[0-9]*.?[0-9]+',
          y = [
            { name: 'px', regexp: new RegExp('^'.concat(p, 'px$')) },
            { name: '%', regexp: new RegExp('^'.concat(p, '%$')) },
            { name: 'px', regexp: new RegExp('^'.concat(p, '$')) }
          ]
        function g(e, t) {
          var n,
            i = Object.keys(e)
          return (
            Object.getOwnPropertySymbols &&
              ((n = Object.getOwnPropertySymbols(e)),
              t &&
                (n = n.filter(function (t) {
                  return Object.getOwnPropertyDescriptor(e, t).enumerable
                })),
              i.push.apply(i, n)),
            i
          )
        }
        function w(i) {
          for (var t = 1; t < arguments.length; t++) {
            var o = null != arguments[t] ? arguments[t] : {}
            t % 2
              ? g(Object(o), !0).forEach(function (t) {
                  var e, n
                  ;(e = i),
                    (t = o[(n = t)]),
                    n in e
                      ? Object.defineProperty(e, n, { value: t, enumerable: !0, configurable: !0, writable: !0 })
                      : (e[n] = t)
                })
              : Object.getOwnPropertyDescriptors
              ? Object.defineProperties(i, Object.getOwnPropertyDescriptors(o))
              : g(Object(o)).forEach(function (t) {
                  Object.defineProperty(i, t, Object.getOwnPropertyDescriptor(o, t))
                })
          }
          return i
        }
        i = f(
          {
            name: 'VueJsModal',
            props: {
              name: { required: !0, type: String },
              delay: { type: Number, default: 0 },
              resizable: { type: Boolean, default: !1 },
              adaptive: { type: Boolean, default: !1 },
              draggable: { type: [Boolean, String], default: !1 },
              scrollable: { type: Boolean, default: !1 },
              reset: { type: Boolean, default: !1 },
              overlayTransition: { type: String, default: 'overlay-fade' },
              transition: { type: String },
              clickToClose: { type: Boolean, default: !0 },
              classes: { type: [String, Array], default: 'v--modal' },
              styles: { type: [String, Array, Object] },
              minWidth: {
                type: Number,
                default: 0,
                validator: function (t) {
                  return 0 <= t
                }
              },
              minHeight: {
                type: Number,
                default: 0,
                validator: function (t) {
                  return 0 <= t
                }
              },
              maxWidth: { type: Number, default: Number.MAX_SAFE_INTEGER },
              maxHeight: { type: Number, default: Number.MAX_SAFE_INTEGER },
              width: { type: [Number, String], default: 600, validator: b },
              height: {
                type: [Number, String],
                default: 300,
                validator: function (t) {
                  return 'auto' === t || b(t)
                }
              },
              pivotX: {
                type: Number,
                default: 0.5,
                validator: function (t) {
                  return 0 <= t && t <= 1
                }
              },
              pivotY: {
                type: Number,
                default: 0.5,
                validator: function (t) {
                  return 0 <= t && t <= 1
                }
              }
            },
            components: { Resizer: o },
            data: function () {
              return {
                visible: !1,
                visibility: { modal: !1, overlay: !1 },
                shift: { left: 0, top: 0 },
                modal: { width: 0, widthType: 'px', height: 0, heightType: 'px', renderedHeight: 0 },
                viewportHeight: 0,
                viewportWidth: 0,
                mutationObserver: null
              }
            },
            created: function () {
              this.setInitialSize()
            },
            beforeMount: function () {
              var t,
                e = this
              k.event.$on('toggle', this.handleToggleEvent),
                window.addEventListener('resize', this.handleWindowResize),
                this.handleWindowResize(),
                this.scrollable &&
                  !this.isAutoHeight &&
                  console.warn(
                    'Modal "'.concat(this.name, '" has scrollable flag set to true ') +
                      'but height is not "auto" ('.concat(this.height, ')')
                  ),
                this.isAutoHeight &&
                  ((t = (function () {
                    if ('undefined' != typeof window)
                      for (var t = ['', 'WebKit', 'Moz', 'O', 'Ms'], e = 0; e < t.length; e++) {
                        var n = t[e] + 'MutationObserver'
                        if (n in window) return window[n]
                      }
                    return !1
                  })())
                    ? (this.mutationObserver = new t(function (t) {
                        e.updateRenderedHeight()
                      }))
                    : console.warn(
                        'MutationObserver was not found. Vue-js-modal automatic resizing relies heavily on MutationObserver. Please make sure to provide shim for it.'
                      )),
                this.clickToClose && window.addEventListener('keyup', this.handleEscapeKeyUp)
            },
            beforeDestroy: function () {
              k.event.$off('toggle', this.handleToggleEvent),
                window.removeEventListener('resize', this.handleWindowResize),
                this.clickToClose && window.removeEventListener('keyup', this.handleEscapeKeyUp),
                this.scrollable && document.body.classList.remove('v--modal-block-scroll')
            },
            computed: {
              isAutoHeight: function () {
                return 'auto' === this.modal.heightType
              },
              position: function () {
                var t = this.viewportHeight,
                  e = this.viewportWidth,
                  n = this.shift,
                  i = this.pivotX,
                  o = this.pivotY,
                  r = this.trueModalWidth,
                  a = this.trueModalHeight,
                  r = e - r,
                  a = Math.max(t - a, 0),
                  i = n.left + i * r,
                  o = n.top + o * a
                return { left: parseInt(c(0, r, i)), top: parseInt(c(0, a, o)) }
              },
              trueModalWidth: function () {
                var t = this.viewportWidth,
                  e = this.modal,
                  n = this.adaptive,
                  i = this.minWidth,
                  o = this.maxWidth,
                  e = '%' === e.widthType ? (t / 100) * e.width : e.width,
                  o = Math.max(i, Math.min(t, o))
                return n ? c(i, o, e) : e
              },
              trueModalHeight: function () {
                var t = this.viewportHeight,
                  e = this.modal,
                  n = this.isAutoHeight,
                  i = this.adaptive,
                  o = this.minHeight,
                  r = this.maxHeight,
                  e = '%' === e.heightType ? (t / 100) * e.height : e.height
                if (n) return this.modal.renderedHeight
                r = Math.max(o, Math.min(t, r))
                return i ? c(o, r, e) : e
              },
              overlayClass: function () {
                return { 'v--modal-overlay': !0, scrollable: this.scrollable && this.isAutoHeight }
              },
              modalClass: function () {
                return ['v--modal-box', this.classes]
              },
              stylesProp: function () {
                return 'string' == typeof this.styles
                  ? this.styles
                      .split(';')
                      .map(function (t) {
                        return t.trim()
                      })
                      .filter(Boolean)
                      .map(function (t) {
                        return t.split(':')
                      })
                      .reduce(function (t, e) {
                        var n = r(e, 2),
                          e = n[0],
                          n = n[1]
                        return l(l({}, t), {}, u({}, e, n))
                      }, {})
                  : this.styles
              },
              modalStyle: function () {
                return [
                  this.stylesProp,
                  {
                    top: this.position.top + 'px',
                    left: this.position.left + 'px',
                    width: this.trueModalWidth + 'px',
                    height: this.isAutoHeight ? 'auto' : this.trueModalHeight + 'px'
                  }
                ]
              }
            },
            watch: {
              visible: function (t) {
                var e = this
                t
                  ? ((this.visibility.overlay = !0),
                    setTimeout(function () {
                      ;(e.visibility.modal = !0),
                        e.$nextTick(function () {
                          e.addDraggableListeners(), e.callAfterEvent(!0)
                        })
                    }, this.delay))
                  : ((this.visibility.modal = !1),
                    setTimeout(function () {
                      ;(e.visibility.overlay = !1),
                        e.$nextTick(function () {
                          e.removeDraggableListeners(), e.callAfterEvent(!1)
                        })
                    }, this.delay))
              }
            },
            methods: {
              handleToggleEvent: function (t, e, n) {
                this.name === t && ((e = void 0 === e ? !this.visible : e), this.toggle(e, n))
              },
              setInitialSize: function () {
                var t = this.modal,
                  e = v(this.width),
                  n = v(this.height)
                ;(t.width = e.value), (t.widthType = e.type), (t.height = n.value), (t.heightType = n.type)
              },
              handleEscapeKeyUp: function (t) {
                27 === t.which && this.visible && this.$modal.hide(this.name)
              },
              handleWindowResize: function () {
                ;(this.viewportWidth = d()), (this.viewportHeight = window.innerHeight), this.ensureShiftInWindowBounds()
              },
              createModalEvent: function () {
                var t = 0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : {}
                return (function () {
                  var t = 0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : {}
                  return l({ id: h(), timestamp: Date.now(), canceled: !1 }, t)
                })(w({ name: this.name, ref: this.$refs.modal }, t))
              },
              handleModalResize: function (t) {
                ;(this.modal.widthType = 'px'),
                  (this.modal.width = t.size.width),
                  (this.modal.heightType = 'px'),
                  (this.modal.height = t.size.height)
                t = this.modal.size
                this.$emit('resize', this.createModalEvent({ size: t }))
              },
              toggle: function (t, e) {
                var n,
                  i = this.reset,
                  o = this.scrollable,
                  r = this.visible
                r !== t &&
                  ('before-open' == (r = r ? 'before-close' : 'before-open')
                    ? ('undefined' != typeof document &&
                        document.activeElement &&
                        'BODY' !== document.activeElement.tagName &&
                        document.activeElement.blur &&
                        document.activeElement.blur(),
                      i && (this.setInitialSize(), (this.shift.left = 0), (this.shift.top = 0)),
                      o && document.body.classList.add('v--modal-block-scroll'))
                    : o && document.body.classList.remove('v--modal-block-scroll'),
                  (n = !1),
                  (e = this.createModalEvent({
                    stop: function () {
                      n = !0
                    },
                    state: t,
                    params: e
                  })),
                  this.$emit(r, e),
                  n || (this.visible = t))
              },
              getDraggableElement: function () {
                var t = 'string' != typeof this.draggable ? '.v--modal-box' : this.draggable
                return t ? this.$refs.overlay.querySelector(t) : null
              },
              handleBackgroundClick: function () {
                this.clickToClose && this.toggle(!1)
              },
              callAfterEvent: function (t) {
                t ? this.connectObserver() : this.disconnectObserver()
                var e = t ? 'opened' : 'closed',
                  t = this.createModalEvent({ state: t })
                this.$emit(e, t)
              },
              addDraggableListeners: function () {
                var t,
                  i,
                  o,
                  r,
                  a,
                  s,
                  e,
                  n,
                  l,
                  u = this
                !this.draggable ||
                  ((t = this.getDraggableElement()) &&
                    ((a = r = o = i = 0),
                    (s = function (t) {
                      return t.touches && 0 < t.touches.length ? t.touches[0] : t
                    }),
                    (e = function (t) {
                      var e = t.target
                      ;(e && ('INPUT' === e.nodeName || 'TEXTAREA' === e.nodeName || 'SELECT' === e.nodeName)) ||
                        ((t = (e = s(t)).clientX),
                        (e = e.clientY),
                        document.addEventListener('mousemove', n),
                        document.addEventListener('touchmove', n),
                        document.addEventListener('mouseup', l),
                        document.addEventListener('touchend', l),
                        (i = t),
                        (o = e),
                        (r = u.shift.left),
                        (a = u.shift.top))
                    }),
                    (n = function (t) {
                      var e = s(t),
                        n = e.clientX,
                        e = e.clientY
                      ;(u.shift.left = r + n - i), (u.shift.top = a + e - o), t.preventDefault()
                    }),
                    (l = function t(e) {
                      u.ensureShiftInWindowBounds(),
                        document.removeEventListener('mousemove', n),
                        document.removeEventListener('touchmove', n),
                        document.removeEventListener('mouseup', t),
                        document.removeEventListener('touchend', t),
                        e.preventDefault()
                    }),
                    t.addEventListener('mousedown', e),
                    t.addEventListener('touchstart', e)))
              },
              removeDraggableListeners: function () {},
              updateRenderedHeight: function () {
                this.$refs.modal && (this.modal.renderedHeight = this.$refs.modal.getBoundingClientRect().height)
              },
              connectObserver: function () {
                this.mutationObserver &&
                  this.mutationObserver.observe(this.$refs.overlay, { childList: !0, attributes: !0, subtree: !0 })
              },
              disconnectObserver: function () {
                this.mutationObserver && this.mutationObserver.disconnect()
              },
              beforeTransitionEnter: function () {
                this.connectObserver()
              },
              afterTransitionEnter: function () {},
              afterTransitionLeave: function () {},
              ensureShiftInWindowBounds: function () {
                var t = this.viewportHeight,
                  e = this.viewportWidth,
                  n = this.shift,
                  i = this.pivotX,
                  o = this.pivotY,
                  r = this.trueModalWidth,
                  a = this.trueModalHeight,
                  r = e - r,
                  a = Math.max(t - a, 0),
                  i = n.left + i * r,
                  o = n.top + o * a
                ;(this.shift.left -= i - c(0, r, i)), (this.shift.top -= o - c(0, a, o))
              }
            }
          },
          i,
          [],
          !1,
          function (t) {
            var e = n(4)
            e.__inject__ && e.__inject__(t)
          },
          null,
          '66524b9d'
        )
        i.options.__file = 'src/Modal.vue'
        var O = i.exports,
          i = function () {
            var n = this,
              t = n.$createElement,
              i = n._self._c || t
            return i(
              'modal',
              {
                attrs: {
                  name: 'dialog',
                  height: 'auto',
                  classes: ['v--modal', 'vue-dialog', this.params.class],
                  width: n.width,
                  'pivot-y': 0.3,
                  adaptive: !0,
                  clickToClose: n.clickToClose,
                  transition: n.transition
                },
                on: {
                  'before-open': n.beforeOpened,
                  'before-close': n.beforeClosed,
                  opened: function (t) {
                    return n.$emit('opened', t)
                  },
                  closed: function (t) {
                    return n.$emit('closed', t)
                  }
                }
              },
              [
                i(
                  'div',
                  { staticClass: 'dialog-content' },
                  [
                    n.params.title
                      ? i('div', { staticClass: 'dialog-c-title', domProps: { innerHTML: n._s(n.params.title || '') } })
                      : n._e(),
                    n._v(' '),
                    n.params.component
                      ? i(n.params.component, n._b({ tag: 'component' }, 'component', n.params.props, !1))
                      : i('div', { staticClass: 'dialog-c-text', domProps: { innerHTML: n._s(n.params.text || '') } })
                  ],
                  1
                ),
                n._v(' '),
                n.buttons
                  ? i(
                      'div',
                      { staticClass: 'vue-dialog-buttons' },
                      n._l(n.buttons, function (t, e) {
                        return i(
                          'button',
                          {
                            key: e,
                            class: t.class || 'vue-dialog-button',
                            style: n.buttonStyle,
                            attrs: { type: 'button' },
                            domProps: { innerHTML: n._s(t.title) },
                            on: {
                              click: function (t) {
                                return t.stopPropagation(), n.click(e, t)
                              }
                            }
                          },
                          [n._v('\n      ' + n._s(t.title) + '\n    ')]
                        )
                      }),
                      0
                    )
                  : i('div', { staticClass: 'vue-dialog-buttons-none' })
              ]
            )
          }
        i._withStripped = !0
        i = f(
          {
            name: 'VueJsDialog',
            props: {
              width: { type: [Number, String], default: 400 },
              clickToClose: { type: Boolean, default: !0 },
              transition: { type: String, default: 'fade' }
            },
            data: function () {
              return { params: {}, defaultButtons: [{ title: 'CLOSE' }] }
            },
            computed: {
              buttons: function () {
                return this.params.buttons || this.defaultButtons
              },
              buttonStyle: function () {
                return { flex: '1 1 '.concat(100 / this.buttons.length, '%') }
              }
            },
            methods: {
              beforeOpened: function (t) {
                window.addEventListener('keyup', this.onKeyUp), (this.params = t.params || {}), this.$emit('before-opened', t)
              },
              beforeClosed: function (t) {
                window.removeEventListener('keyup', this.onKeyUp), (this.params = {}), this.$emit('before-closed', t)
              },
              click: function (t, e) {
                var n = 2 < arguments.length && void 0 !== arguments[2] ? arguments[2] : 'click',
                  i = this.buttons[t]
                i && 'function' == typeof i.handler ? i.handler(t, e, { source: n }) : this.$modal.hide('dialog')
              },
              onKeyUp: function (t) {
                var e
                13 === t.which &&
                  0 < this.buttons.length &&
                  -1 !==
                    (e =
                      1 === this.buttons.length
                        ? 0
                        : this.buttons.findIndex(function (t) {
                            return t.default
                          })) &&
                  this.click(e, t, 'keypress')
              }
            }
          },
          i,
          [],
          !1,
          function (t) {
            var e = n(5)
            e.__inject__ && e.__inject__(t)
          },
          null,
          '6a29aa08'
        )
        i.options.__file = 'src/Dialog.vue'
        var _ = i.exports,
          i = function () {
            var n = this,
              t = n.$createElement,
              i = n._self._c || t
            return i(
              'div',
              { attrs: { id: 'modals-container' } },
              n._l(n.modals, function (e) {
                return i(
                  'modal',
                  n._g(
                    n._b(
                      {
                        key: e.id,
                        on: {
                          closed: function (t) {
                            return n.remove(e.id)
                          }
                        }
                      },
                      'modal',
                      e.modalAttrs,
                      !1
                    ),
                    e.modalListeners
                  ),
                  [
                    i(
                      e.component,
                      n._g(
                        n._b(
                          {
                            tag: 'component',
                            on: {
                              close: function (t) {
                                return n.$modal.hide(e.modalAttrs.name, t)
                              }
                            }
                          },
                          'component',
                          e.componentAttrs,
                          !1
                        ),
                        n.$listeners
                      )
                    )
                  ],
                  1
                )
              }),
              1
            )
          }
        function E(e, t) {
          var n,
            i = Object.keys(e)
          return (
            Object.getOwnPropertySymbols &&
              ((n = Object.getOwnPropertySymbols(e)),
              t &&
                (n = n.filter(function (t) {
                  return Object.getOwnPropertyDescriptor(e, t).enumerable
                })),
              i.push.apply(i, n)),
            i
          )
        }
        function j(i) {
          for (var t = 1; t < arguments.length; t++) {
            var o = null != arguments[t] ? arguments[t] : {}
            t % 2
              ? E(Object(o), !0).forEach(function (t) {
                  var e, n
                  ;(e = i),
                    (t = o[(n = t)]),
                    n in e
                      ? Object.defineProperty(e, n, { value: t, enumerable: !0, configurable: !0, writable: !0 })
                      : (e[n] = t)
                })
              : Object.getOwnPropertyDescriptors
              ? Object.defineProperties(i, Object.getOwnPropertyDescriptors(o))
              : E(Object(o)).forEach(function (t) {
                  Object.defineProperty(i, t, Object.getOwnPropertyDescriptor(o, t))
                })
          }
          return i
        }
        i._withStripped = !0
        i = f(
          {
            data: function () {
              return { modals: [] }
            },
            created: function () {
              this.$root._dynamicContainer = this
            },
            methods: {
              add: function (t) {
                var e = this,
                  n = 1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : {},
                  i = 2 < arguments.length && void 0 !== arguments[2] ? arguments[2] : {},
                  o = 3 < arguments.length && void 0 !== arguments[3] ? arguments[3] : {},
                  r = h(),
                  a = i.name || '_dynamic_modal_' + r
                this.modals.push({
                  id: r,
                  modalAttrs: j(j({}, i), {}, { name: a }),
                  modalListeners: o,
                  component: t,
                  componentAttrs: n
                }),
                  this.$nextTick(function () {
                    e.$modal.show(a)
                  })
              },
              remove: function (e) {
                var t = this.modals.findIndex(function (t) {
                  return t.id === e
                })
                ;-1 !== t && this.modals.splice(t, 1)
              }
            }
          },
          i,
          [],
          !1,
          null,
          null,
          '699720aa'
        )
        i.options.__file = 'src/ModalsContainer.vue'
        var x = i.exports
        function S(t) {
          return (S =
            'function' == typeof Symbol && 'symbol' == typeof Symbol.iterator
              ? function (t) {
                  return typeof t
                }
              : function (t) {
                  return t && 'function' == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype
                    ? 'symbol'
                    : typeof t
                })(t)
        }
        function T(e, t) {
          var n,
            i = Object.keys(e)
          return (
            Object.getOwnPropertySymbols &&
              ((n = Object.getOwnPropertySymbols(e)),
              t &&
                (n = n.filter(function (t) {
                  return Object.getOwnPropertyDescriptor(e, t).enumerable
                })),
              i.push.apply(i, n)),
            i
          )
        }
        function M(i) {
          for (var t = 1; t < arguments.length; t++) {
            var o = null != arguments[t] ? arguments[t] : {}
            t % 2
              ? T(Object(o), !0).forEach(function (t) {
                  var e, n
                  ;(e = i),
                    (t = o[(n = t)]),
                    n in e
                      ? Object.defineProperty(e, n, { value: t, enumerable: !0, configurable: !0, writable: !0 })
                      : (e[n] = t)
                })
              : Object.getOwnPropertyDescriptors
              ? Object.defineProperties(i, Object.getOwnPropertyDescriptors(o))
              : T(Object(o)).forEach(function (t) {
                  Object.defineProperty(i, t, Object.getOwnPropertyDescriptor(o, t))
                })
          }
          return i
        }
        var P = function (t, e, n) {
            return (
              !n._dynamicContainer &&
                e.injectModalsContainer &&
                ((e = document.createElement('div')),
                document.body.appendChild(e),
                (e = e),
                new t({
                  parent: n,
                  render: function (t) {
                    return t(x)
                  }
                }).$mount(e)),
              n._dynamicContainer
            )
          },
          $ = {
            install: function (r) {
              var t,
                a,
                s = 1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : {}
              this.installed ||
                ((this.installed = !0),
                (this.event = new r()),
                (this.rootInstance = null),
                (t = s.componentName || 'Modal'),
                (a = s.dynamicDefaults || {}),
                (r.prototype.$modal = {
                  show: function (t) {
                    for (var e = arguments.length, n = new Array(1 < e ? e - 1 : 0), i = 1; i < e; i++) n[i - 1] = arguments[i]
                    switch (S(t)) {
                      case 'string':
                        return function (t, e) {
                          $.event.$emit('toggle', t, !0, e)
                        }.apply(void 0, [t].concat(n))
                      case 'object':
                      case 'function':
                        return s.dynamic
                          ? function (t, e, n, i) {
                              var o = n && n.root ? n.root : $.rootInstance,
                                o = P(r, s, o)
                              o
                                ? o.add(t, e, M(M({}, a), n), i)
                                : console.warn(
                                    '[vue-js-modal] In order to render dynamic modals, a <modals-container> component must be present on the page.'
                                  )
                            }.apply(void 0, [t].concat(n))
                          : console.warn(
                              '[vue-js-modal] $modal() received object as a first argument, but dynamic modals are switched off. https://github.com/euvl/vue-js-modal/#dynamic-modals'
                            )
                      default:
                        console.warn('[vue-js-modal] $modal() received an unsupported argument as a first argument.', t)
                    }
                  },
                  hide: function (t, e) {
                    $.event.$emit('toggle', t, !1, e)
                  },
                  toggle: function (t, e) {
                    $.event.$emit('toggle', t, void 0, e)
                  }
                }),
                r.component(t, O),
                s.dialog && r.component('VDialog', _),
                s.dynamic &&
                  (r.component('ModalsContainer', x),
                  r.mixin({
                    beforeMount: function () {
                      null === $.rootInstance && ($.rootInstance = this.$root)
                    }
                  })))
            }
          },
          k = (e.default = $)
      }
    ]),
    (o.c = i),
    (o.d = function (t, e, n) {
      o.o(t, e) || Object.defineProperty(t, e, { enumerable: !0, get: n })
    }),
    (o.r = function (t) {
      'undefined' != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, { value: 'Module' }),
        Object.defineProperty(t, '__esModule', { value: !0 })
    }),
    (o.t = function (e, t) {
      if ((1 & t && (e = o(e)), 8 & t)) return e
      if (4 & t && 'object' == typeof e && e && e.__esModule) return e
      var n = Object.create(null)
      if ((o.r(n), Object.defineProperty(n, 'default', { enumerable: !0, value: e }), 2 & t && 'string' != typeof e))
        for (var i in e)
          o.d(
            n,
            i,
            function (t) {
              return e[t]
            }.bind(null, i)
          )
      return n
    }),
    (o.n = function (t) {
      var e =
        t && t.__esModule
          ? function () {
              return t.default
            }
          : function () {
              return t
            }
      return o.d(e, 'a', e), e
    }),
    (o.o = function (t, e) {
      return Object.prototype.hasOwnProperty.call(t, e)
    }),
    (o.p = '/dist/'),
    o((o.s = 6))
  )
  function o(t) {
    if (i[t]) return i[t].exports
    var e = (i[t] = { i: t, l: !1, exports: {} })
    return n[t].call(e.exports, e, e.exports, o), (e.l = !0), e.exports
  }
  var n, i
})
