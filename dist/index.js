!(function (e, t) {
  'object' == typeof exports && 'object' == typeof module
    ? (module.exports = t())
    : 'function' == typeof define && define.amd
    ? define([], t)
    : 'object' == typeof exports
    ? (exports['vue-js-modal'] = t())
    : (e['vue-js-modal'] = t())
})(window, function () {
  return (
    (r = {}),
    (o.m = n = [
      function (e, t, n) {
        var r = n(6)
        'string' == typeof r && (r = [[e.i, r, '']]), r.locals && (e.exports = r.locals)
        ;(0, n(4).default)('27d83796', r, !1, {})
      },
      function (e, t, n) {
        var r = n(8)
        'string' == typeof r && (r = [[e.i, r, '']]), r.locals && (e.exports = r.locals)
        ;(0, n(4).default)('0e783494', r, !1, {})
      },
      function (e, t, n) {
        var r = n(10)
        'string' == typeof r && (r = [[e.i, r, '']]), r.locals && (e.exports = r.locals)
        ;(0, n(4).default)('17757f60', r, !1, {})
      },
      function (e, t) {
        e.exports = function (n) {
          var a = []
          return (
            (a.toString = function () {
              return this.map(function (e) {
                var t = (function (e, t) {
                  var n = e[1] || '',
                    r = e[3]
                  if (!r) return n
                  if (t && 'function' == typeof btoa) {
                    var o = (function (e) {
                        return (
                          '/*# sourceMappingURL=data:application/json;charset=utf-8;base64,' +
                          btoa(unescape(encodeURIComponent(JSON.stringify(e)))) +
                          ' */'
                        )
                      })(r),
                      i = r.sources.map(function (e) {
                        return '/*# sourceURL=' + r.sourceRoot + e + ' */'
                      })
                    return [n].concat(i).concat([o]).join('\n')
                  }
                  return [n].join('\n')
                })(e, n)
                return e[2] ? '@media ' + e[2] + '{' + t + '}' : t
              }).join('')
            }),
            (a.i = function (e, t) {
              'string' == typeof e && (e = [[null, e, '']])
              for (var n = {}, r = 0; r < this.length; r++) {
                var o = this[r][0]
                'number' == typeof o && (n[o] = !0)
              }
              for (r = 0; r < e.length; r++) {
                var i = e[r]
                ;('number' == typeof i[0] && n[i[0]]) ||
                  (t && !i[2] ? (i[2] = t) : t && (i[2] = '(' + i[2] + ') and (' + t + ')'), a.push(i))
              }
            }),
            a
          )
        }
      },
      function (e, t, n) {
        'use strict'
        function l(e, t) {
          for (var n = [], r = {}, o = 0; o < t.length; o++) {
            var i = t[o],
              a = i[0],
              s = { id: e + ':' + o, css: i[1], media: i[2], sourceMap: i[3] }
            r[a] ? r[a].parts.push(s) : n.push((r[a] = { id: a, parts: [s] }))
          }
          return n
        }
        n.r(t),
          n.d(t, 'default', function () {
            return p
          })
        var r = 'undefined' != typeof document
        if ('undefined' != typeof DEBUG && DEBUG && !r)
          throw new Error(
            "vue-style-loader cannot be used in a non-browser environment. Use { target: 'node' } in your Webpack config to indicate a server-rendering environment."
          )
        var u = {},
          o = r && (document.head || document.getElementsByTagName('head')[0]),
          i = null,
          a = 0,
          d = !1,
          s = function () {},
          c = null,
          f = 'data-vue-ssr-id',
          h = 'undefined' != typeof navigator && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase())
        function p(a, e, t, n) {
          ;(d = t), (c = n || {})
          var s = l(a, e)
          return (
            m(s),
            function (e) {
              for (var t = [], n = 0; n < s.length; n++) {
                var r = s[n]
                ;(o = u[r.id]).refs--, t.push(o)
              }
              e ? m((s = l(a, e))) : (s = [])
              for (n = 0; n < t.length; n++) {
                var o
                if (0 === (o = t[n]).refs) {
                  for (var i = 0; i < o.parts.length; i++) o.parts[i]()
                  delete u[o.id]
                }
              }
            }
          )
        }
        function m(e) {
          for (var t = 0; t < e.length; t++) {
            var n = e[t],
              r = u[n.id]
            if (r) {
              r.refs++
              for (var o = 0; o < r.parts.length; o++) r.parts[o](n.parts[o])
              for (; o < n.parts.length; o++) r.parts.push(g(n.parts[o]))
              r.parts.length > n.parts.length && (r.parts.length = n.parts.length)
            } else {
              var i = []
              for (o = 0; o < n.parts.length; o++) i.push(g(n.parts[o]))
              u[n.id] = { id: n.id, refs: 1, parts: i }
            }
          }
        }
        function v() {
          var e = document.createElement('style')
          return (e.type = 'text/css'), o.appendChild(e), e
        }
        function g(t) {
          var n,
            r,
            e = document.querySelector('style[' + f + '~="' + t.id + '"]')
          if (e) {
            if (d) return s
            e.parentNode.removeChild(e)
          }
          if (h) {
            var o = a++
            ;(e = i = i || v()), (n = w.bind(null, e, o, !1)), (r = w.bind(null, e, o, !0))
          } else
            (e = v()),
              (n = function (e, t) {
                var n = t.css,
                  r = t.media,
                  o = t.sourceMap
                r && e.setAttribute('media', r)
                c.ssrId && e.setAttribute(f, t.id)
                o &&
                  ((n += '\n/*# sourceURL=' + o.sources[0] + ' */'),
                  (n +=
                    '\n/*# sourceMappingURL=data:application/json;base64,' +
                    btoa(unescape(encodeURIComponent(JSON.stringify(o)))) +
                    ' */'))
                if (e.styleSheet) e.styleSheet.cssText = n
                else {
                  for (; e.firstChild; ) e.removeChild(e.firstChild)
                  e.appendChild(document.createTextNode(n))
                }
              }.bind(null, e)),
              (r = function () {
                e.parentNode.removeChild(e)
              })
          return (
            n(t),
            function (e) {
              if (e) {
                if (e.css === t.css && e.media === t.media && e.sourceMap === t.sourceMap) return
                n((t = e))
              } else r()
            }
          )
        }
        var b,
          y =
            ((b = []),
            function (e, t) {
              return (b[e] = t), b.filter(Boolean).join('\n')
            })
        function w(e, t, n, r) {
          var o = n ? '' : r.css
          if (e.styleSheet) e.styleSheet.cssText = y(t, o)
          else {
            var i = document.createTextNode(o),
              a = e.childNodes
            a[t] && e.removeChild(a[t]), a.length ? e.insertBefore(i, a[t]) : e.appendChild(i)
          }
        }
      },
      function (e, t, n) {
        'use strict'
        var r = n(0)
        n.n(r).a
      },
      function (e, t, n) {
        ;(e.exports = n(3)(!1)).push([
          e.i,
          "\n.vue-modal-resizer {\r\n  display: block;\r\n  overflow: hidden;\r\n  position: absolute;\r\n  width: 12px;\r\n  height: 12px;\r\n  right: 0;\r\n  bottom: 0;\r\n  z-index: 9999999;\r\n  background: transparent;\r\n  cursor: se-resize;\n}\n.vue-modal-resizer::after {\r\n  display: block;\r\n  position: absolute;\r\n  content: '';\r\n  background: transparent;\r\n  left: 0;\r\n  top: 0;\r\n  width: 0;\r\n  height: 0;\r\n  border-bottom: 10px solid #ddd;\r\n  border-left: 10px solid transparent;\n}\n.vue-modal-resizer.clicked::after {\r\n  border-bottom: 10px solid #369be9;\n}\r\n",
          ''
        ])
      },
      function (e, t, n) {
        'use strict'
        var r = n(1)
        n.n(r).a
      },
      function (e, t, n) {
        ;(e.exports = n(3)(!1)).push([
          e.i,
          '\n.v--modal-block-scroll {\r\n  overflow: hidden;\r\n  width: 100vw;\n}\n.v--modal-overlay {\r\n  position: fixed;\r\n  box-sizing: border-box;\r\n  left: 0;\r\n  top: 0;\r\n  width: 100%;\r\n  height: 100vh;\r\n  background: rgba(0, 0, 0, 0.2);\r\n  z-index: 999;\r\n  opacity: 1;\n}\n.v--modal-overlay.scrollable {\r\n  height: 100%;\r\n  min-height: 100vh;\r\n  overflow-y: auto;\r\n  -webkit-overflow-scrolling: touch;\n}\n.v--modal-overlay .v--modal-background-click {\r\n  width: 100%;\r\n  min-height: 100%;\r\n  height: auto;\n}\n.v--modal-overlay .v--modal-box {\r\n  position: relative;\r\n  overflow: hidden;\r\n  box-sizing: border-box;\n}\n.v--modal-overlay.scrollable .v--modal-box {\r\n  margin-bottom: 2px;\n}\n.v--modal {\r\n  background-color: white;\r\n  text-align: left;\r\n  border-radius: 3px;\r\n  box-shadow: 0 20px 60px -2px rgba(27, 33, 58, 0.4);\r\n  padding: 0;\n}\n.v--modal.v--modal-fullscreen {\r\n  width: 100vw;\r\n  height: 100vh;\r\n  margin: 0;\r\n  left: 0;\r\n  top: 0;\n}\n.v--modal-top-right {\r\n  display: block;\r\n  position: absolute;\r\n  right: 0;\r\n  top: 0;\n}\n.overlay-fade-enter-active,\r\n.overlay-fade-leave-active {\r\n  transition: all 0.2s;\n}\n.overlay-fade-enter,\r\n.overlay-fade-leave-active {\r\n  opacity: 0;\n}\n.nice-modal-fade-enter-active,\r\n.nice-modal-fade-leave-active {\r\n  transition: all 0.4s;\n}\n.nice-modal-fade-enter,\r\n.nice-modal-fade-leave-active {\r\n  opacity: 0;\r\n  transform: translateY(-20px);\n}\r\n',
          ''
        ])
      },
      function (e, t, n) {
        'use strict'
        var r = n(2)
        n.n(r).a
      },
      function (e, t, n) {
        ;(e.exports = n(3)(!1)).push([
          e.i,
          '\n.vue-dialog div {\r\n  box-sizing: border-box;\n}\n.vue-dialog .dialog-flex {\r\n  width: 100%;\r\n  height: 100%;\n}\n.vue-dialog .dialog-content {\r\n  flex: 1 0 auto;\r\n  width: 100%;\r\n  padding: 15px;\r\n  font-size: 14px;\n}\n.vue-dialog .dialog-c-title {\r\n  font-weight: 600;\r\n  padding-bottom: 15px;\n}\n.vue-dialog .dialog-c-text {\n}\n.vue-dialog .vue-dialog-buttons {\r\n  display: flex;\r\n  flex: 0 1 auto;\r\n  width: 100%;\r\n  border-top: 1px solid #eee;\n}\n.vue-dialog .vue-dialog-buttons-none {\r\n  width: 100%;\r\n  padding-bottom: 15px;\n}\n.vue-dialog-button {\r\n  font-size: 12px !important;\r\n  background: transparent;\r\n  padding: 0;\r\n  margin: 0;\r\n  border: 0;\r\n  cursor: pointer;\r\n  box-sizing: border-box;\r\n  line-height: 40px;\r\n  height: 40px;\r\n  color: inherit;\r\n  font: inherit;\r\n  outline: none;\n}\n.vue-dialog-button:hover {\r\n  background: rgba(0, 0, 0, 0.01);\n}\n.vue-dialog-button:active {\r\n  background: rgba(0, 0, 0, 0.025);\n}\n.vue-dialog-button:not(:first-of-type) {\r\n  border-left: 1px solid #eee;\n}\r\n',
          ''
        ])
      },
      function (e, t, n) {
        'use strict'
        n.r(t),
          n.d(t, 'getModalsContainer', function () {
            return I
          })
        function r() {
          var t = this,
            e = t.$createElement,
            n = t._self._c || e
          return n('transition', { attrs: { name: t.overlayTransition } }, [
            t.visibility.overlay
              ? n(
                  'div',
                  {
                    ref: 'overlay',
                    class: t.overlayClass,
                    attrs: { 'aria-expanded': t.visibility.overlay.toString(), 'data-modal': t.name }
                  },
                  [
                    n(
                      'div',
                      {
                        staticClass: 'v--modal-background-click',
                        on: {
                          mousedown: function (e) {
                            return e.target !== e.currentTarget ? null : t.handleBackgroundClick(e)
                          },
                          touchstart: function (e) {
                            return e.target !== e.currentTarget ? null : t.handleBackgroundClick(e)
                          }
                        }
                      },
                      [
                        n('div', { staticClass: 'v--modal-top-right' }, [t._t('top-right')], 2),
                        t._v(' '),
                        n(
                          'transition',
                          {
                            attrs: { name: t.transition },
                            on: {
                              'before-enter': t.beforeTransitionEnter,
                              'after-enter': t.afterTransitionEnter,
                              'after-leave': t.afterTransitionLeave
                            }
                          },
                          [
                            t.visibility.modal
                              ? n(
                                  'div',
                                  {
                                    ref: 'modal',
                                    class: t.modalClass,
                                    style: t.modalStyle,
                                    attrs: { role: 'dialog', 'aria-modal': 'true' }
                                  },
                                  [
                                    t._t('default'),
                                    t._v(' '),
                                    t.resizable && !t.isAutoHeight
                                      ? n('resizer', {
                                          attrs: {
                                            'min-width': t.minWidth,
                                            'min-height': t.minHeight,
                                            'max-width': t.maxWidth,
                                            'max-height': t.maxHeight
                                          },
                                          on: { resize: t.handleModalResize }
                                        })
                                      : t._e()
                                  ],
                                  2
                                )
                              : t._e()
                          ]
                        )
                      ],
                      1
                    )
                  ]
                )
              : t._e()
          ])
        }
        function o() {
          var e = this.$createElement
          return (this._self._c || e)('div', { class: this.className })
        }
        function i(e, t) {
          return (
            (function (e) {
              if (Array.isArray(e)) return e
            })(e) ||
            (function (e, t) {
              if ('undefined' == typeof Symbol || !(Symbol.iterator in Object(e))) return
              var n = [],
                r = !0,
                o = !1,
                i = void 0
              try {
                for (
                  var a, s = e[Symbol.iterator]();
                  !(r = (a = s.next()).done) && (n.push(a.value), !t || n.length !== t);
                  r = !0
                );
              } catch (e) {
                ;(o = !0), (i = e)
              } finally {
                try {
                  r || null == s.return || s.return()
                } finally {
                  if (o) throw i
                }
              }
              return n
            })(e, t) ||
            (function (e, t) {
              if (!e) return
              if ('string' == typeof e) return a(e, t)
              var n = Object.prototype.toString.call(e).slice(8, -1)
              'Object' === n && e.constructor && (n = e.constructor.name)
              if ('Map' === n || 'Set' === n) return Array.from(e)
              if ('Arguments' === n || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return a(e, t)
            })(e, t) ||
            (function () {
              throw new TypeError(
                'Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.'
              )
            })()
          )
        }
        function a(e, t) {
          ;(null == t || t > e.length) && (t = e.length)
          for (var n = 0, r = new Array(t); n < t; n++) r[n] = e[n]
          return r
        }
        function s(t, e) {
          var n = Object.keys(t)
          if (Object.getOwnPropertySymbols) {
            var r = Object.getOwnPropertySymbols(t)
            e &&
              (r = r.filter(function (e) {
                return Object.getOwnPropertyDescriptor(t, e).enumerable
              })),
              n.push.apply(n, r)
          }
          return n
        }
        function l(t) {
          for (var e = 1; e < arguments.length; e++) {
            var n = null != arguments[e] ? arguments[e] : {}
            e % 2
              ? s(Object(n), !0).forEach(function (e) {
                  u(t, e, n[e])
                })
              : Object.getOwnPropertyDescriptors
              ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(n))
              : s(Object(n)).forEach(function (e) {
                  Object.defineProperty(t, e, Object.getOwnPropertyDescriptor(n, e))
                })
          }
          return t
        }
        function u(e, t, n) {
          return (
            t in e ? Object.defineProperty(e, t, { value: n, enumerable: !0, configurable: !0, writable: !0 }) : (e[t] = n), e
          )
        }
        o._withStripped = r._withStripped = !0
        function c(e, t, n) {
          return n < e ? e : t < n ? t : n
        }
        function d() {
          var e = window.innerWidth,
            t = document.documentElement.clientWidth
          return e && t ? Math.min(e, t) : t || e
        }
        var f = (function (e) {
            var t = 0 < arguments.length && void 0 !== e ? e : 0
            return function () {
              return (t++).toString()
            }
          })(),
          h = {
            name: 'VueJsModalResizer',
            props: {
              minHeight: { type: Number, default: 0 },
              minWidth: { type: Number, default: 0 },
              maxWidth: { type: Number, default: Number.MAX_SAFE_INTEGER },
              maxHeight: { type: Number, default: Number.MAX_SAFE_INTEGER }
            },
            data: function () {
              return { clicked: !1, size: {} }
            },
            mounted: function () {
              this.$el.addEventListener('mousedown', this.start, !1)
            },
            computed: {
              className: function () {
                return { 'vue-modal-resizer': !0, clicked: this.clicked }
              }
            },
            methods: {
              start: function (e) {
                ;(this.clicked = !0),
                  window.addEventListener('mousemove', this.mousemove, !1),
                  window.addEventListener('mouseup', this.stop, !1),
                  e.stopPropagation(),
                  e.preventDefault()
              },
              stop: function () {
                ;(this.clicked = !1),
                  window.removeEventListener('mousemove', this.mousemove, !1),
                  window.removeEventListener('mouseup', this.stop, !1),
                  this.$emit('resize-stop', { element: this.$el.parentElement, size: this.size })
              },
              mousemove: function (e) {
                this.resize(e)
              },
              resize: function (e) {
                var t = this.$el.parentElement
                if (t) {
                  var n = e.clientX - t.offsetLeft,
                    r = e.clientY - t.offsetTop,
                    o = Math.min(d(), this.maxWidth),
                    i = Math.min(window.innerHeight, this.maxHeight)
                  ;(n = c(this.minWidth, o, n)),
                    (r = c(this.minHeight, i, r)),
                    (this.size = { width: n, height: r }),
                    (t.style.width = n + 'px'),
                    (t.style.height = r + 'px'),
                    this.$emit('resize', { element: t, size: this.size })
                }
              }
            }
          }
        n(5)
        function p(e, t, n, r, o, i, a, s) {
          var l,
            u = 'function' == typeof e ? e.options : e
          if (
            (t && ((u.render = t), (u.staticRenderFns = n), (u._compiled = !0)),
            r && (u.functional = !0),
            i && (u._scopeId = 'data-v-' + i),
            a
              ? ((l = function (e) {
                  ;(e =
                    e ||
                    (this.$vnode && this.$vnode.ssrContext) ||
                    (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext)) ||
                    'undefined' == typeof __VUE_SSR_CONTEXT__ ||
                    (e = __VUE_SSR_CONTEXT__),
                    o && o.call(this, e),
                    e && e._registeredComponents && e._registeredComponents.add(a)
                }),
                (u._ssrRegister = l))
              : o &&
                (l = s
                  ? function () {
                      o.call(this, this.$root.$options.shadowRoot)
                    }
                  : o),
            l)
          )
            if (u.functional) {
              u._injectStyles = l
              var d = u.render
              u.render = function (e, t) {
                return l.call(t), d(e, t)
              }
            } else {
              var c = u.beforeCreate
              u.beforeCreate = c ? [].concat(c, l) : [l]
            }
          return { exports: e, options: u }
        }
        var m = p(h, o, [], !1, null, null, null)
        m.options.__file = 'src/Resizer.vue'
        var v = m.exports
        function g(e) {
          return (g =
            'function' == typeof Symbol && 'symbol' == typeof Symbol.iterator
              ? function (e) {
                  return typeof e
                }
              : function (e) {
                  return e && 'function' == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype
                    ? 'symbol'
                    : typeof e
                })(e)
        }
        function b(e) {
          switch (g(e)) {
            case 'number':
              return { type: 'px', value: e }
            case 'string':
              return (function (t) {
                if ('auto' === t) return { type: t, value: 0 }
                var e = x.find(function (e) {
                  return e.regexp.test(t)
                })
                return e ? { type: e.name, value: parseFloat(t) } : { type: '', value: t }
              })(e)
            default:
              return { type: '', value: e }
          }
        }
        function y(e) {
          if ('string' != typeof e) return 0 <= e
          var t = b(e)
          return ('%' === t.type || 'px' === t.type) && 0 < t.value
        }
        var w = '[-+]?[0-9]*.?[0-9]+',
          x = [
            { name: 'px', regexp: new RegExp('^'.concat(w, 'px$')) },
            { name: '%', regexp: new RegExp('^'.concat(w, '%$')) },
            { name: 'px', regexp: new RegExp('^'.concat(w, '$')) }
          ]
        function O(t, e) {
          var n = Object.keys(t)
          if (Object.getOwnPropertySymbols) {
            var r = Object.getOwnPropertySymbols(t)
            e &&
              (r = r.filter(function (e) {
                return Object.getOwnPropertyDescriptor(t, e).enumerable
              })),
              n.push.apply(n, r)
          }
          return n
        }
        function E(e, t, n) {
          return (
            t in e ? Object.defineProperty(e, t, { value: n, enumerable: !0, configurable: !0, writable: !0 }) : (e[t] = n), e
          )
        }
        var j = {
            name: 'VueJsModal',
            props: {
              name: { required: !0, type: String },
              delay: { type: Number, default: 0 },
              resizable: { type: Boolean, default: !1 },
              adaptive: { type: Boolean, default: !1 },
              draggable: { type: [Boolean, String], default: !1 },
              scrollable: { type: Boolean, default: !1 },
              reset: { type: Boolean, default: !1 },
              overlayTransition: { type: String, default: 'overlay-fade' },
              transition: { type: String },
              clickToClose: { type: Boolean, default: !0 },
              classes: { type: [String, Array], default: 'v--modal' },
              styles: { type: [String, Array, Object] },
              minWidth: {
                type: Number,
                default: 0,
                validator: function (e) {
                  return 0 <= e
                }
              },
              minHeight: {
                type: Number,
                default: 0,
                validator: function (e) {
                  return 0 <= e
                }
              },
              maxWidth: { type: Number, default: Number.MAX_SAFE_INTEGER },
              maxHeight: { type: Number, default: Number.MAX_SAFE_INTEGER },
              width: { type: [Number, String], default: 600, validator: y },
              height: {
                type: [Number, String],
                default: 300,
                validator: function (e) {
                  return 'auto' === e || y(e)
                }
              },
              pivotX: {
                type: Number,
                default: 0.5,
                validator: function (e) {
                  return 0 <= e && e <= 1
                }
              },
              pivotY: {
                type: Number,
                default: 0.5,
                validator: function (e) {
                  return 0 <= e && e <= 1
                }
              }
            },
            components: { Resizer: v },
            data: function () {
              return {
                visible: !1,
                visibility: { modal: !1, overlay: !1 },
                shift: { left: 0, top: 0 },
                modal: { width: 0, widthType: 'px', height: 0, heightType: 'px', renderedHeight: 0 },
                viewportHeight: 0,
                viewportWidth: 0,
                mutationObserver: null
              }
            },
            created: function () {
              this.setInitialSize()
            },
            beforeMount: function () {
              var t = this
              if (
                (U.event.$on('toggle', this.handleToggleEvent),
                window.addEventListener('resize', this.handleWindowResize),
                this.handleWindowResize(),
                this.scrollable &&
                  !this.isAutoHeight &&
                  console.warn(
                    'Modal "'.concat(this.name, '" has scrollable flag set to true ') +
                      'but height is not "auto" ('.concat(this.height, ')')
                  ),
                this.isAutoHeight)
              ) {
                var e = (function () {
                  if ('undefined' != typeof window)
                    for (var e = ['', 'WebKit', 'Moz', 'O', 'Ms'], t = 0; t < e.length; t++) {
                      var n = e[t] + 'MutationObserver'
                      if (n in window) return window[n]
                    }
                  return !1
                })()
                e
                  ? (this.mutationObserver = new e(function (e) {
                      t.updateRenderedHeight()
                    }))
                  : console.warn(
                      'MutationObserver was not found. Vue-js-modal automatic resizing relies heavily on MutationObserver. Please make sure to provide shim for it.'
                    )
              }
              this.clickToClose && window.addEventListener('keyup', this.handleEscapeKeyUp)
            },
            beforeDestroy: function () {
              U.event.$off('toggle', this.handleToggleEvent),
                window.removeEventListener('resize', this.handleWindowResize),
                this.clickToClose && window.removeEventListener('keyup', this.handleEscapeKeyUp),
                this.scrollable && document.body.classList.remove('v--modal-block-scroll')
            },
            computed: {
              isAutoHeight: function () {
                return 'auto' === this.modal.heightType
              },
              position: function () {
                var e = this.viewportHeight,
                  t = this.viewportWidth,
                  n = this.shift,
                  r = this.pivotX,
                  o = this.pivotY,
                  i = this.trueModalWidth,
                  a = this.trueModalHeight,
                  s = t - i,
                  l = Math.max(e - a, 0),
                  u = n.left + r * s,
                  d = n.top + o * l
                return { left: parseInt(c(0, s, u)), top: parseInt(c(0, l, d)) }
              },
              trueModalWidth: function () {
                var e = this.viewportWidth,
                  t = this.modal,
                  n = this.adaptive,
                  r = this.minWidth,
                  o = this.maxWidth,
                  i = '%' === t.widthType ? (e / 100) * t.width : t.width,
                  a = Math.max(r, Math.min(e, o))
                return n ? c(r, a, i) : i
              },
              trueModalHeight: function () {
                var e = this.viewportHeight,
                  t = this.modal,
                  n = this.isAutoHeight,
                  r = this.adaptive,
                  o = this.minHeight,
                  i = this.maxHeight,
                  a = '%' === t.heightType ? (e / 100) * t.height : t.height
                if (n) return this.modal.renderedHeight
                var s = Math.max(o, Math.min(e, i))
                return r ? c(o, s, a) : a
              },
              overlayClass: function () {
                return { 'v--modal-overlay': !0, scrollable: this.scrollable && this.isAutoHeight }
              },
              modalClass: function () {
                return ['v--modal-box', this.classes]
              },
              stylesProp: function () {
                return 'string' == typeof this.styles
                  ? this.styles
                      .split(';')
                      .map(function (e) {
                        return e.trim()
                      })
                      .filter(Boolean)
                      .map(function (e) {
                        return e.split(':')
                      })
                      .reduce(function (e, t) {
                        var n = i(t, 2),
                          r = n[0],
                          o = n[1]
                        return l(l({}, e), {}, u({}, r, o))
                      }, {})
                  : this.styles
              },
              modalStyle: function () {
                return [
                  this.stylesProp,
                  {
                    top: this.position.top + 'px',
                    left: this.position.left + 'px',
                    width: this.trueModalWidth + 'px',
                    height: this.isAutoHeight ? 'auto' : this.trueModalHeight + 'px'
                  }
                ]
              }
            },
            watch: {
              visible: function (e) {
                var t = this
                e
                  ? ((this.visibility.overlay = !0),
                    setTimeout(function () {
                      ;(t.visibility.modal = !0),
                        t.$nextTick(function () {
                          t.addDraggableListeners(), t.callAfterEvent(!0)
                        })
                    }, this.delay))
                  : ((this.visibility.modal = !1),
                    setTimeout(function () {
                      ;(t.visibility.overlay = !1),
                        t.$nextTick(function () {
                          t.removeDraggableListeners(), t.callAfterEvent(!1)
                        })
                    }, this.delay))
              }
            },
            methods: {
              handleToggleEvent: function (e, t, n) {
                if (this.name === e) {
                  var r = void 0 === t ? !this.visible : t
                  this.toggle(r, n)
                }
              },
              setInitialSize: function () {
                var e = this.modal,
                  t = b(this.width),
                  n = b(this.height)
                ;(e.width = t.value), (e.widthType = t.type), (e.height = n.value), (e.heightType = n.type)
              },
              handleEscapeKeyUp: function (e) {
                27 === e.which && this.visible && this.$modal.hide(this.name)
              },
              handleWindowResize: function () {
                ;(this.viewportWidth = d()), (this.viewportHeight = window.innerHeight), this.ensureShiftInWindowBounds()
              },
              createModalEvent: function (e) {
                var t = 0 < arguments.length && void 0 !== e ? e : {}
                return (function (e) {
                  var t = 0 < arguments.length && void 0 !== e ? e : {}
                  return l({ id: f(), timestamp: Date.now(), canceled: !1 }, t)
                })(
                  (function (t) {
                    for (var e = 1; e < arguments.length; e++) {
                      var n = null != arguments[e] ? arguments[e] : {}
                      e % 2
                        ? O(Object(n), !0).forEach(function (e) {
                            E(t, e, n[e])
                          })
                        : Object.getOwnPropertyDescriptors
                        ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(n))
                        : O(Object(n)).forEach(function (e) {
                            Object.defineProperty(t, e, Object.getOwnPropertyDescriptor(n, e))
                          })
                    }
                    return t
                  })({ name: this.name, ref: this.$refs.modal }, t)
                )
              },
              handleModalResize: function (e) {
                ;(this.modal.widthType = 'px'),
                  (this.modal.width = e.size.width),
                  (this.modal.heightType = 'px'),
                  (this.modal.height = e.size.height)
                var t = this.modal.size
                this.$emit('resize', this.createModalEvent({ size: t }))
              },
              toggle: function (e, t) {
                var n = this.reset,
                  r = this.scrollable,
                  o = this.visible
                if (o !== e) {
                  var i = o ? 'before-close' : 'before-open'
                  'before-open' == i
                    ? ('undefined' != typeof document &&
                        document.activeElement &&
                        'BODY' !== document.activeElement.tagName &&
                        document.activeElement.blur &&
                        document.activeElement.blur(),
                      n && (this.setInitialSize(), (this.shift.left = 0), (this.shift.top = 0)),
                      r && document.body.classList.add('v--modal-block-scroll'))
                    : r && document.body.classList.remove('v--modal-block-scroll')
                  var a = !1,
                    s = this.createModalEvent({
                      stop: function () {
                        a = !0
                      },
                      state: e,
                      params: t
                    })
                  this.$emit(i, s), a || (this.visible = e)
                }
              },
              getDraggableElement: function () {
                var e = 'string' != typeof this.draggable ? '.v--modal-box' : this.draggable
                return e ? this.$refs.overlay.querySelector(e) : null
              },
              handleBackgroundClick: function () {
                this.clickToClose && this.toggle(!1)
              },
              callAfterEvent: function (e) {
                e ? this.connectObserver() : this.disconnectObserver()
                var t = e ? 'opened' : 'closed',
                  n = this.createModalEvent({ state: e })
                this.$emit(t, n)
              },
              addDraggableListeners: function () {
                var i = this
                if (this.draggable) {
                  var e = this.getDraggableElement()
                  if (e) {
                    var a = 0,
                      s = 0,
                      l = 0,
                      u = 0,
                      d = function (e) {
                        return e.touches && 0 < e.touches.length ? e.touches[0] : e
                      },
                      t = function (e) {
                        var t = e.target
                        if (!t || ('INPUT' !== t.nodeName && 'TEXTAREA' !== t.nodeName && 'SELECT' !== t.nodeName)) {
                          var n = d(e),
                            r = n.clientX,
                            o = n.clientY
                          document.addEventListener('mousemove', c),
                            document.addEventListener('touchmove', c),
                            document.addEventListener('mouseup', f),
                            document.addEventListener('touchend', f),
                            (a = r),
                            (s = o),
                            (l = i.shift.left),
                            (u = i.shift.top)
                        }
                      },
                      c = function (e) {
                        var t = d(e),
                          n = t.clientX,
                          r = t.clientY
                        ;(i.shift.left = l + n - a), (i.shift.top = u + r - s), e.preventDefault()
                      },
                      f = function e(t) {
                        i.ensureShiftInWindowBounds(),
                          document.removeEventListener('mousemove', c),
                          document.removeEventListener('touchmove', c),
                          document.removeEventListener('mouseup', e),
                          document.removeEventListener('touchend', e),
                          t.preventDefault()
                      }
                    e.addEventListener('mousedown', t), e.addEventListener('touchstart', t)
                  }
                }
              },
              removeDraggableListeners: function () {},
              updateRenderedHeight: function () {
                this.$refs.modal && (this.modal.renderedHeight = this.$refs.modal.getBoundingClientRect().height)
              },
              connectObserver: function () {
                this.mutationObserver &&
                  this.mutationObserver.observe(this.$refs.overlay, { childList: !0, attributes: !0, subtree: !0 })
              },
              disconnectObserver: function () {
                this.mutationObserver && this.mutationObserver.disconnect()
              },
              beforeTransitionEnter: function () {
                this.connectObserver()
              },
              afterTransitionEnter: function () {},
              afterTransitionLeave: function () {},
              ensureShiftInWindowBounds: function () {
                var e = this.viewportHeight,
                  t = this.viewportWidth,
                  n = this.shift,
                  r = this.pivotX,
                  o = this.pivotY,
                  i = this.trueModalWidth,
                  a = this.trueModalHeight,
                  s = t - i,
                  l = Math.max(e - a, 0),
                  u = n.left + r * s,
                  d = n.top + o * l
                ;(this.shift.left -= u - c(0, s, u)), (this.shift.top -= d - c(0, l, d))
              }
            }
          },
          _ = (n(7), p(j, r, [], !1, null, null, null))
        _.options.__file = 'src/Modal.vue'
        function S() {
          var n = this,
            e = n.$createElement,
            r = n._self._c || e
          return r(
            'modal',
            {
              attrs: {
                name: 'dialog',
                height: 'auto',
                classes: ['v--modal', 'vue-dialog', this.params.class],
                width: n.width,
                'pivot-y': 0.3,
                adaptive: !0,
                clickToClose: n.clickToClose,
                transition: n.transition
              },
              on: {
                'before-open': n.beforeOpened,
                'before-close': n.beforeClosed,
                opened: function (e) {
                  n.$emit('opened', e)
                },
                closed: function (e) {
                  n.$emit('closed', e)
                }
              }
            },
            [
              r(
                'div',
                { staticClass: 'dialog-content' },
                [
                  n.params.title
                    ? r('div', { staticClass: 'dialog-c-title', domProps: { innerHTML: n._s(n.params.title || '') } })
                    : n._e(),
                  n._v(' '),
                  n.params.component
                    ? r(n.params.component, n._b({ tag: 'component' }, 'component', n.params.props, !1))
                    : r('div', { staticClass: 'dialog-c-text', domProps: { innerHTML: n._s(n.params.text || '') } })
                ],
                1
              ),
              n._v(' '),
              n.buttons
                ? r(
                    'div',
                    { staticClass: 'vue-dialog-buttons' },
                    n._l(n.buttons, function (e, t) {
                      return r(
                        'button',
                        {
                          key: t,
                          class: e.class || 'vue-dialog-button',
                          style: n.buttonStyle,
                          attrs: { type: 'button' },
                          domProps: { innerHTML: n._s(e.title) },
                          on: {
                            click: function (e) {
                              e.stopPropagation(), n.click(t, e)
                            }
                          }
                        },
                        [n._v('\n      ' + n._s(e.title) + '\n    ')]
                      )
                    })
                  )
                : r('div', { staticClass: 'vue-dialog-buttons-none' })
            ]
          )
        }
        var k = _.exports
        S._withStripped = !0
        var T = {
            name: 'VueJsDialog',
            props: {
              width: { type: [Number, String], default: 400 },
              clickToClose: { type: Boolean, default: !0 },
              transition: { type: String, default: 'fade' }
            },
            data: function () {
              return { params: {}, defaultButtons: [{ title: 'CLOSE' }] }
            },
            computed: {
              buttons: function () {
                return this.params.buttons || this.defaultButtons
              },
              buttonStyle: function () {
                return { flex: '1 1 '.concat(100 / this.buttons.length, '%') }
              }
            },
            methods: {
              beforeOpened: function (e) {
                window.addEventListener('keyup', this.onKeyUp), (this.params = e.params || {}), this.$emit('before-opened', e)
              },
              beforeClosed: function (e) {
                window.removeEventListener('keyup', this.onKeyUp), (this.params = {}), this.$emit('before-closed', e)
              },
              click: function (e, t, n) {
                var r = 2 < arguments.length && void 0 !== n ? n : 'click',
                  o = this.buttons[e]
                o && 'function' == typeof o.handler ? o.handler(e, t, { source: r }) : this.$modal.hide('dialog')
              },
              onKeyUp: function (e) {
                if (13 === e.which && 0 < this.buttons.length) {
                  var t =
                    1 === this.buttons.length
                      ? 0
                      : this.buttons.findIndex(function (e) {
                          return e.default
                        })
                  ;-1 !== t && this.click(t, e, 'keypress')
                }
              }
            }
          },
          M = (n(9), p(T, S, [], !1, null, null, null))
        M.options.__file = 'src/Dialog.vue'
        function C() {
          var n = this,
            e = n.$createElement,
            r = n._self._c || e
          return r(
            'div',
            { attrs: { id: 'modals-container' } },
            n._l(n.modals, function (t) {
              return r(
                'modal',
                n._g(
                  n._b(
                    {
                      key: t.id,
                      on: {
                        closed: function (e) {
                          n.remove(t.id)
                        }
                      }
                    },
                    'modal',
                    t.modalAttrs,
                    !1
                  ),
                  t.modalListeners
                ),
                [
                  r(
                    t.component,
                    n._g(
                      n._b(
                        {
                          tag: 'component',
                          on: {
                            close: function (e) {
                              n.$modal.hide(t.modalAttrs.name, e)
                            }
                          }
                        },
                        'component',
                        t.componentAttrs,
                        !1
                      ),
                      n.$listeners
                    )
                  )
                ],
                1
              )
            })
          )
        }
        var P = M.exports
        function $(t, e) {
          var n = Object.keys(t)
          if (Object.getOwnPropertySymbols) {
            var r = Object.getOwnPropertySymbols(t)
            e &&
              (r = r.filter(function (e) {
                return Object.getOwnPropertyDescriptor(t, e).enumerable
              })),
              n.push.apply(n, r)
          }
          return n
        }
        function z(t) {
          for (var e = 1; e < arguments.length; e++) {
            var n = null != arguments[e] ? arguments[e] : {}
            e % 2
              ? $(Object(n), !0).forEach(function (e) {
                  L(t, e, n[e])
                })
              : Object.getOwnPropertyDescriptors
              ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(n))
              : $(Object(n)).forEach(function (e) {
                  Object.defineProperty(t, e, Object.getOwnPropertyDescriptor(n, e))
                })
          }
          return t
        }
        function L(e, t, n) {
          return (
            t in e ? Object.defineProperty(e, t, { value: n, enumerable: !0, configurable: !0, writable: !0 }) : (e[t] = n), e
          )
        }
        C._withStripped = !0
        var N = p(
          {
            data: function () {
              return { modals: [] }
            },
            created: function () {
              this.$root._dynamicContainer = this
            },
            methods: {
              add: function (e, t, n, r) {
                var o = this,
                  i = 1 < arguments.length && void 0 !== t ? t : {},
                  a = 2 < arguments.length && void 0 !== n ? n : {},
                  s = 3 < arguments.length && void 0 !== r ? r : {},
                  l = f(),
                  u = a.name || '_dynamic_modal_' + l
                this.modals.push({
                  id: l,
                  modalAttrs: z(z({}, a), {}, { name: u }),
                  modalListeners: s,
                  component: e,
                  componentAttrs: i
                }),
                  this.$nextTick(function () {
                    o.$modal.show(u)
                  })
              },
              remove: function (t) {
                var e = this.modals.findIndex(function (e) {
                  return e.id === t
                })
                ;-1 !== e && this.modals.splice(e, 1)
              }
            }
          },
          C,
          [],
          !1,
          null,
          null,
          null
        )
        N.options.__file = 'src/ModalsContainer.vue'
        var A = N.exports
        function H(e) {
          return (H =
            'function' == typeof Symbol && 'symbol' == typeof Symbol.iterator
              ? function (e) {
                  return typeof e
                }
              : function (e) {
                  return e && 'function' == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype
                    ? 'symbol'
                    : typeof e
                })(e)
        }
        function D(t, e) {
          var n = Object.keys(t)
          if (Object.getOwnPropertySymbols) {
            var r = Object.getOwnPropertySymbols(t)
            e &&
              (r = r.filter(function (e) {
                return Object.getOwnPropertyDescriptor(t, e).enumerable
              })),
              n.push.apply(n, r)
          }
          return n
        }
        function R(t) {
          for (var e = 1; e < arguments.length; e++) {
            var n = null != arguments[e] ? arguments[e] : {}
            e % 2
              ? D(Object(n), !0).forEach(function (e) {
                  W(t, e, n[e])
                })
              : Object.getOwnPropertyDescriptors
              ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(n))
              : D(Object(n)).forEach(function (e) {
                  Object.defineProperty(t, e, Object.getOwnPropertyDescriptor(n, e))
                })
          }
          return t
        }
        function W(e, t, n) {
          return (
            t in e ? Object.defineProperty(e, t, { value: n, enumerable: !0, configurable: !0, writable: !0 }) : (e[t] = n), e
          )
        }
        var I = function (e, t, n) {
            if (!n._dynamicContainer && t.injectModalsContainer) {
              var r = ((o = document.createElement('div')), document.body.appendChild(o), o)
              new e({
                parent: n,
                render: function (e) {
                  return e(A)
                }
              }).$mount(r)
            }
            var o
            return n._dynamicContainer
          },
          B = {
            install: function (a, e) {
              var s = 1 < arguments.length && void 0 !== e ? e : {}
              if (!this.installed) {
                ;(this.installed = !0), (this.event = new a()), (this.rootInstance = null)
                var t = s.componentName || 'Modal',
                  l = s.dynamicDefaults || {}
                ;(a.prototype.$modal = {
                  show: function (e) {
                    for (var t = arguments.length, n = new Array(1 < t ? t - 1 : 0), r = 1; r < t; r++) n[r - 1] = arguments[r]
                    switch (H(e)) {
                      case 'string':
                        return function (e, t) {
                          B.event.$emit('toggle', e, !0, t)
                        }.apply(void 0, [e].concat(n))
                      case 'object':
                      case 'function':
                        return s.dynamic
                          ? function (e, t, n, r) {
                              var o = n && n.root ? n.root : B.rootInstance,
                                i = I(a, s, o)
                              i
                                ? i.add(e, t, R(R({}, l), n), r)
                                : console.warn(
                                    '[vue-js-modal] In order to render dynamic modals, a <modals-container> component must be present on the page.'
                                  )
                            }.apply(void 0, [e].concat(n))
                          : console.warn(
                              '[vue-js-modal] $modal() received object as a first argument, but dynamic modals are switched off. https://github.com/euvl/vue-js-modal/#dynamic-modals'
                            )
                      default:
                        console.warn('[vue-js-modal] $modal() received an unsupported argument as a first argument.', e)
                    }
                  },
                  hide: function (e, t) {
                    B.event.$emit('toggle', e, !1, t)
                  },
                  toggle: function (e, t) {
                    B.event.$emit('toggle', e, void 0, t)
                  }
                }),
                  a.component(t, k),
                  s.dialog && a.component('VDialog', P),
                  s.dynamic &&
                    (a.component('ModalsContainer', A),
                    a.mixin({
                      beforeMount: function () {
                        null === B.rootInstance && (B.rootInstance = this.$root)
                      }
                    }))
              }
            }
          },
          U = (t.default = B)
      }
    ]),
    (o.c = r),
    (o.d = function (e, t, n) {
      o.o(e, t) || Object.defineProperty(e, t, { enumerable: !0, get: n })
    }),
    (o.r = function (e) {
      'undefined' != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, { value: 'Module' }),
        Object.defineProperty(e, '__esModule', { value: !0 })
    }),
    (o.t = function (t, e) {
      if ((1 & e && (t = o(t)), 8 & e)) return t
      if (4 & e && 'object' == typeof t && t && t.__esModule) return t
      var n = Object.create(null)
      if ((o.r(n), Object.defineProperty(n, 'default', { enumerable: !0, value: t }), 2 & e && 'string' != typeof t))
        for (var r in t)
          o.d(
            n,
            r,
            function (e) {
              return t[e]
            }.bind(null, r)
          )
      return n
    }),
    (o.n = function (e) {
      var t =
        e && e.__esModule
          ? function () {
              return e.default
            }
          : function () {
              return e
            }
      return o.d(t, 'a', t), t
    }),
    (o.o = function (e, t) {
      return Object.prototype.hasOwnProperty.call(e, t)
    }),
    (o.p = '/dist/'),
    o((o.s = 11))
  )
  function o(e) {
    if (r[e]) return r[e].exports
    var t = (r[e] = { i: e, l: !1, exports: {} })
    return n[e].call(t.exports, t, t.exports, o), (t.l = !0), t.exports
  }
  var n, r
})
